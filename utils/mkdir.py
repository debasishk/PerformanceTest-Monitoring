import errno
import os
import sys

def mkdir(path):
    """
    mkdir makes a directory in a safe way with respect to race conditions. It respects the python version.

    Args:
        path:

    Returns:
    None
    """
    if sys.version_info[0] == 2:
        try:
            os.makedirs(path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise
    else:
        os.makedirs(path, exist_ok=True)
