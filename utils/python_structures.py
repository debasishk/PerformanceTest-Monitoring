"""
    Implementation method 1
"""

from ctypes import *

class StructureType(Structure):
    _fields_ = [("var1", c_int),
                ("var2", c_int),
                ("var3", c_float),
                ("var4", c_bool)]


var = StructureType(1, 4, 1.1, True)

print(var.var1)
print(var.var2)
print(var.var3)
print(var.var4)

"""------------------------------------------------------------"""

