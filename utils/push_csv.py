from utils.dbCon import connect_db
import os
import pandas as pd


def pushCSV(file=None, tbl=None):
    if file is None:
        raise ValueError("Please pass valid file name to be pushed. Passed value is none")
    elif tbl is None:
        raise ValueError("Please pass valid table name into which data to be pushed. Passed value is none")
    else:
        if os.path.isfile(file):
            ffound = True
        else:
            ffound = False
            raise FileNotFoundError("File doesnt exist. Kindly check passed path. Passed path is {}".format(file))

        if ffound:
            print("Pushing {} file to {} table".format(file, tbl))

            con = connect_db('write', package='sqlalchemy', assist=True)
            host = '127.0.0.1'
            usr = 'root'
            pwd = ''
            db = 'performancetesting'
            uri = 'mysql+pymysql://root:@localhost/performancetesting?charset=utf8&local_infile=1'
            con.set_params(host=host, user=usr, db=db, pwd=pwd, uri=uri)

            df = pd.read_csv(file)

            dt = df['timestamp'].values.tolist()
            dt = [pd.to_datetime(elem) for elem in dt]
            df['timestamp'] = dt
            df.set_index('timestamp', inplace=True)

            ret = con.push_data(df, tbl, True)

            if type(ret) is bool:
                if ret:
                    print("Table pushed successfully")
            else:
                raise AssertionError(ret)


if __name__ == '__main__':
    fname = 'pt_metrics.csv'
    tbl = 'pt_metrics'

    pushCSV(fname, tbl)
