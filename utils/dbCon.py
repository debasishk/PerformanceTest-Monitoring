"""
Database connection layer. Use connect_db.
"""

import pymysql
import MySQLdb
import sqlalchemy
import pandas as pd
import datetime
import os
import traceback
import datetime


class DBCon(object):
    """
    Connection class to govern data access layer.
    """
    def __init__(self, kind, environment='prod', component='engine', package='pymysql'):
        self.kind = kind
        self.environment = environment
        self.component = component
        self.package = package
        self.fmt = "%m-%d-%y %H:%M:%S"
        self.tbl_fmt = "%Y-%m-%d %H:%M:%S"
        self.read_tbl = ['SELECT', 'SHOW']

        self.selection = self.get_selection()

        self.host, self.ip, self.db, self.user, self.pwd, self.uri = self.get_credentials(self.selection)

        pass

    def connect_db(self):
        """
        connect_db method to return database class depending on module/package used.

        Returns:
            db (DataBase object): The Database connection object
        """
        if self.package == 'pymysql':
            db = self.py_mysql()
        elif self.package == 'mysqldb':
            db = self.mysql_db()
        elif self.package == 'sqlalchemy':
            db = self.sql_alchemy()
        return db

    def get_selection(self):
        """
        Method to auto initialize type of DB's host in use. Can be either pa/dashboard db and prod/dev primary/secondary hosts.

        Returns:
            selection (String): The selected host type inferred.
        """
        if self.component == 'engine':
            if self.environment == 'prod':
                if self.kind == 'read':
                    selection = 'pa_prod_primary'

                elif self.kind == 'write':
                    selection = 'pa_prod_primary'
            elif self.environment == 'dev':
                if self.kind == 'read':
                    selection = 'pa_dev_primary'

                elif self.kind == 'write':
                    selection = 'pa_dev_primary'

        elif self.component == 'error_etl':
            if self.kind == 'read':
                selection = 'dashboard_primary'

            elif self.kind == 'write':
                selection = 'pa_dev_primary'

        return selection

    def get_credentials(self, selection):
        """
        Get the credentials of the host selected.

        Args:
            selection (String): The host selection/type selected

        Returns:
            host (String): Host name where Database is hosted.
            ip (String): The ip of the host for DB
            db (String): The Database name where information are stored.
            user (String): The user name to login in DB
            pwd (String): The password for login into DB
            uri (String): The URI for creating connection based on SQL Alchemy.
        """
        if selection == 'dashboard_primary':
            host = 'esu3v544'
            ip = '11.120.5.159'
            db = 'mcom_metrics'
            user = 'pa'
            pwd = 'Pr3dict!v@'

        elif selection == 'dashboard_secondary':
            host = 'esu4v136'
            ip = '11.48.169.63'
            db = 'mcom_metrics'
            user = 'pa'
            pwd = 'Pr3dict!v@'

        elif selection == 'pa_prod_primary':
            host = 'esu4v401'
            ip = '11.48.171.11'
            db = 'predictiveAnalytics'
            user = 'pa'
            pwd = 'Pr3dict!v@'

        elif selection == 'pa_prod_secondary':
            host = None
            ip = None
            db = 'predictiveAnalytics'
            user = 'pa'
            pwd = 'Pr3dict!v@'

        elif selection == 'pa_dev_primary':
            host = 'esu1l300'
            ip =  '11.120.36.241'
            db = 'predictiveAnalytics'
            user = 'pa'
            pwd = 'Pr3dict!v@'

        elif selection == 'pa_dev_secondary':
            host = None
            ip = None
            db = 'predictiveAnalytics'
            user = 'pa'
            pwd = 'Pr3dict!v@'

        uri = 'mysql+pymysql://pa:Pr3dict!v@@esu4v401/predictiveAnalytics?charset=utf8&local_infile=1'

        return host, ip, db, user, pwd, uri

    def py_mysql(self):
        """
        Method to create DB connection object based on PyMySQL module.

        Returns:
            db (DB connection object): The DB connection object based on PyMySQL module
        """
        db = pymysql.connect(host=self.host, db=self.db, user=self.user, passwd=self.pwd)
        return db

    def mysql_db(self):
        """
        Method to create DB connection object based on MySQLdb module.

        Returns:
            db (DB connection object): The DB connection object based on MySQLdb module
        """
        db = MySQLdb.Connect(host=self.host, db=self.db, user=self.user, passwd=self.pwd, local_infile=1)
        return db

    def sql_alchemy(self):
        """
        Method to create DB connection object based on SQLalchemy module

        Returns:
           engine (DB connection engine object): The DB connection engine object create based on SQL alchemy module.
        """
        engine = sqlalchemy.create_engine(self.uri)
        return engine

    def __get_schema_from_dict__(self, metadata, cols):
        """
        Generate schema according to metadata passed.

        This is private function for generating schema to this class and not generalized method and hence is metadata specific generator.

        Args:
            metadata (Dict): Dictionary of columns information.
                            Eg: metadata = {'table_name': '{Table_Name}', 'primary_key': 'timestamp',
                                            'timestamp': {'Default': Null, 'datatype': 'datetime', 'Unique': False},
                                            'server_metrics': {'Default': Null, 'datatype': 'float', 'Unique': False} and so on }

        Returns:
            schema (String): Generated schema for table
        """

        schema = ''

        table_name = schema['table_name']

        schema = 'CREATE TABLE IF NOT EXISTS `{}` ('.format(table_name)

        for col in cols:
            col_name = metadata[col]
            col_type = metadata[col]['datatype']
            col_default = metadata[col]['Default']
            col_unique = metadata[col]['Unique']

            if col_unique:
                schema += '`{}` {} DEFAULT {} UNIQUE,'.format(col_name, col_type, col_default)

            else:
                schema += '`{}` {} DEFAULT {},'.format(col_name, col_type)

        primary = metadata['primary_key']
        # The column name which is primary key. If None is passed, no primary keys.

        if primary:
            schema += 'PRIMARY KEY({}))'.format(primary)

        else:
            # When no primary key is passed, remove last comma from schema generated
            schema = schema[:-1]
            schema += ')'

        schema += 'ENGINE=InnoDB DEFAULT CHARSET=latin1;'

        return schema

    def __get_schema_from_df__(self, df, table):
        """
        Generate schema from dataframe passed.

        Args:
            df (Pandas DataFrame): The dataframe where data is stored, and whose columns to be used to generating schema
            table (String): The table name to be used

        Returns:
            schema (String): The final generated schema
        """

        schema = ''

        if 'timestamp' in df.columns.tolist():
            schema = "CREATE TABLE IF NOT EXISTS `{}` ( `timestamp` datetime DEFAULT NULL UNIQUE, ".format(table)

            for column in df.columns.tolist():
                if column != 'timestamp':
                    schema += "`" + column + "` float DEFAULT NULL,"

            schema += "PRIMARY KEY (timestamp)) ENGINE=InnoDB DEFAULT CHARSET=latin1;"

        else:
            schema = "CREATE TABLE IF NOT EXISTS `{}` ( `timestamp` datetime DEFAULT NULL UNIQUE, ".format(table)

            for column in df.columns.tolist():
                schema += "`" + column + "` float DEFAULT NULL,"

            schema += "PRIMARY KEY (timestamp)) ENGINE=InnoDB DEFAULT CHARSET=latin1;"

        return schema

    def create_table(self, metadata, cols=list()):
        """
        This method is used to create table automatically.

        This method is used to create table automatically. Defaults to schema getting creating or metadata object getting created depending on parameter schema.
        if schema is set to Falsa, then information inside metadata is used to create table, else schema is generated.

        Args:
            metadata (Dict): Dictionary of columns information.
                            Eg: metadata = {'timestamp': {'Default': Null, 'datatype': 'datetime', 'Unique': False, 'Primary_Key': True},
                                            'server_metrics': {'Default': Null, 'datatype': 'float', 'Unique': False, 'Primary_Key': False} and so on }
                            Or,
                    (String): The schema for table which is to be used for table creation. Depends on flag schema = [True/False]

            cols (List of Strings): The column names to be created in table. Table with columns names in same chronological order to be created.

        Returns:

        """

        # Auto infer is schema is passed or metadata.
        if type(metadata) is dict:
            schema = False
        elif type(metadata) is str:
            schema = True
        else:
            raise ValueError('Passed type for schema is wrong. Couldn\'t auto infer type. Passed metadata type is {}. \
                                Please see documentation to know argument type.'.format(type(metadata)))

        if not schema:
            create_schema = self.__get_schema_from_dict__(metadata, cols)
        else:
            create_schema = metadata

        # Defaulted to using SQL alchemy always.
        statement = sqlalchemy.sql.text(create_schema)

        engine = self.sql_alchemy()
        con = engine.connect()
        con.execute(statement)

        print ("Table Created")
        pass

    def set_params(self, **kwargs):
        """
        This method is used to set parameters for dbCon class. This is defaulted to use PA database on esu4v401 for SQLalchemy.
        The parameters which can be changed are DB credentials, IPs, and URI for use in SQLalchemy. Please note that the 'keyword' name has to be instance's name in class.

        Examples:
            Allowed Keywords: host, ip, db, user, pwd, uri

            Default parameters can be changed as follows::

                params_to_change = dict()
                params_to_change['host'] = host_value
                params_to_change['user'] = username
                params_to_change['pwd'] = password

                from utils import connect_db

                db = connect_db('read', package='sqlalchemy', assist=True)
                db.set_params(params_to_change)

                **Proceed with further executions then.**

        Args:
            **kwargs: Keyword arguments passed with new parameters into which default parametrs to be changed.

        Keyword Args:
            Keywords are as follows::

            'host': The host name as keyword to kwargs dictionary
            'ip': IP of database as keyword to kwargs dictionary
            'db': The name of database to connect to on host.
            'user': The username of database
            'pwd': The password of database
            'uri': URI of database for creating connection based on SQLalchemy

        Returns:
            True, if method successfully re-initialized parameters. Else,

        Raises:
            ValueError: When passed keyword in kwargs dictionary is not present in allowed keywords.
        """

        allowed_keywords = ['host', 'ip', 'db', 'user', 'pwd', 'uri']

        args_keywords = kwargs.keys()

        error_msg = None
        wrong_keywords = list()
        error_keywords = ''

        for kwd in args_keywords:
            if kwd not in allowed_keywords:
                wrong_keywords.append(kwd)

        if len(wrong_keywords):
            error_keywords = ','.join(wrong_keywords)
            error_msg = 'Wrong keyword argument passed. Passed keyword/s {} not in allowed keyword argument list {}'.format(error_keywords, allowed_keywords)

        if error_msg is not None:
            raise ValueError(error_msg)
        else:
            for keyword in args_keywords:
                setattr(self, keyword, kwargs[keyword])

    def read_table(self, tbl, cols=None, cols_to_order_by=dict()):
        """
        This method is helper method. Available when assist=True. It is used to read table passed by parameters

        Args:
            tbl (String): The table name to be read
            cols (None, or List of Strings): The column names to be read. If None, all columns are read
            cols_to_order_by (Dictionary): The column to be used for ordering data read. Eg: {'timestamp': 'ASC'}

        Returns:
            df (Pandas DataFrame): The table read into DataFrame
        """

        db = self.sql_alchemy()

        # Generate SQL statements to read table

        if len(cols_to_order_by) == 1:
            if cols is None and len(cols_to_order_by) == 0:
                sql = "SELECT * FROM {}".format(tbl)

            elif cols is not None and len(cols_to_order_by) == 0:
                sql = "SELECT "
                for col in cols[:-1]:
                    sql += "{}, ".format(col)
                sql += "{} FROM {}".format(cols[-1], tbl)

            elif cols is not None and len(cols_to_order_by) == 1:
                order_col = cols_to_order_by.keys()[0]
                order_value = cols_to_order_by[order_col]

                sql = "SELECT "
                for col in cols[:-1]:
                    sql += "{}, ".format(col)
                sql += "{} FROM {}".format(cols[-1], tbl)
                sql += " ORDER BY {} {}".format(order_col, order_value)

            elif cols is None and len(cols_to_order_by) == 1:
                order_col = cols_to_order_by.keys()[0]
                order_value = cols_to_order_by[order_col]

                sql = "SELECT * FROM {}".format(tbl)
                sql += " ORDER BY {} {}".format(order_col, order_value)

        else:
            raise AssertionError('There can only be one column by which input data can be ordered by. Passed value is {}.\
                                Example param: {\'timestamp\': \'ASC\'} and passed is {}'.format(len(cols_to_order_by)), cols_to_order_by)

        try:
            # When Timestamp column in present
            df = pd.read_sql(sql=sql, con=db, index_col='timestamp', parse_dates={'timestamp':self.fmt})
        except:
            # Else, this section
            df = pd.read_sql(sql=sql, con=db)

        return df

    def execute_sql(self, sql):
        """
        This is general common function to execute and sql required. The use of PyMySQL & MySQLdb modules to execute sql queries is depriciated, and henceforward only uses SQL alchemy

        Currently supports only 'SELECT' and 'SHOW' for reading from table. More reading keywords can be added in list 'read_tbl'

        Args:
            sql (String): The sql to be executed.

        Returns:
            df (Pandas Dataframe): The table read into DataFrame object. Else, if it is insertion sql statement,
                                    Return True, if successful.

        """

        read = False

        df = pd.DataFrame()

        for elem in self.read_tbl:
            if elem in sql:
                read = True

        if 'timestamp' in sql:
            # Hard coded. If 'index' column is to be fetched. In this case it is 'timestamp' col
            db = self.sql_alchemy()

            df = pd.read_sql(sql=sql, con=db)

        else:
            if self.package == 'pymysql' or self.package == 'mysqldb':
                raise DeprecationWarning('Usage or PyMySQL & MySQLdb modules in helper methods is deprecated. \
                                            The layer has started using SQL alchemy for all helper methods henceforward.')

            db = self.sql_alchemy()
            con = db.connect()

            statement = sqlalchemy.sql.text(sql)

            if read:
                try:
                    df = pd.read_sql(sql=statement, con=db, index_col='timestamp', parse_dates={'timestamp':self.fmt})
                except:
                    # When timestamp column not present in result table fetched
                    df = pd.read_sql(sql=statement, con=db)

            else:
                con.execute(statement)

        if read:
            return df
        else:
            return True

    def check_table(self, table):
        """
        Check if the table exists in database. If yes, pass, else call create table to create table.

        Args:
            table:

        Returns:

        """

        sql = "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '{}' AND table_name='{}'".format(self.db, table)

        result = self.execute_sql(sql)
        result = result['COUNT(*)'].values[0]

        if result == 1:
            # If table exists.
            return True
        else:
            return False

    def uncommon_rows(self, df1, df2, reverse=False):
        """
        This finds and returns the rows of df2, which are not in df1. If you want to find other way round, either pass opposite dataframes, or reverse=False

        Args:
            df1 (Pandas DataFrame): DataFrame 1 from which common rows to be compared with
            df2 (Pandas DataFrame): DataFrame 2 from which uncommon rows to be found out
            reverse (Bool): If True, uncommon rows from df2 is found out, else vice-versa

        Returns:
            df (Pandas DataFrame): The uncommon rows from dataframe to be returned.
        """
        df = pd.DataFrame()
        df1.fillna(value=0.0, inplace=True)
        df2.fillna(value=0.0, inplace=True)
        df2['timestamp'] = df2.index
        ts = df1.index.tolist()
        if type(ts[0]) is str:
            ts = [datetime.datetime.strptime(elem, self.tbl_fmt) for elem in ts]
        df1['timestamp'] = ts

        if reverse:
            common = df1[['timestamp']].merge(df2[['timestamp']], how='inner', on='timestamp')
            mask = (~df2['timestamp'].isin(common['timestamp']))
            df2.drop('timestamp', axis=1, inplace=True)
            df = df2[mask]
        else:
            # common = df1[['timestamp']].merge(df2[['timestamp']], how='inner', on='timestamp')
            common = df1[['timestamp']].merge(df2[['timestamp']], how='inner', on='timestamp')
            mask = (~df1['timestamp'].isin(common['timestamp']))
            df1.drop('timestamp', axis=1, inplace=True)
            df = df1[mask]

        return df

    def push_data(self, data, table, ts_index=True):
        """
        Load data directly into table specified by parameter.

        Currently loads data directly using only SQLalchemy connection. If required, can be extended later on to include other modules also.

        Args:
            data (Pandas DataFrame): Pandas dataframe, indexed by timestamp which will be used as primary key.
                                        If timestamp is present as columns & not index, provive ts_index=False
            table (String): The name of table into which data to be inserted.
            ts_index (Bool): Defaults to True. Implies index of dataframe is timestamp to be used as primary key.
                                Else, there is separate column present in data named timestamp.

        Returns:
            Bool. If Data loading successful, returns True, else 'Error Message'
        """

        if ts_index or (not ts_index and 'timestamp' in data.columns.tolist()):
            tbl_exists = self.check_table(table)

            if tbl_exists:
                # Check if table exists. If it does, then push only [DF_Original - DF_table] set to table.

                read_tbl = "SELECT * FROM `{}` ORDER BY TIMESTAMP ASC".format(table)
                tblDF = self.execute_sql(read_tbl)

                new_data = self.uncommon_rows(data, tblDF)
                # print new_data

                engine = self.sql_alchemy()

                if ts_index:
                    new_data.to_sql(name=table, con=engine, if_exists='append', index=True, index_label='timestamp')
                else:
                    new_data.to_sql(name=table, con=engine, if_exists='append', index=False)

            else:
                schema = self.__get_schema_from_df__(data, table)

                self.create_table(schema)

                engine = self.sql_alchemy()

                if ts_index:
                    # ts = data.index.tolist()
                    # data.reset_index(drop=True, inplace=True)
                    # data['timestamp'] = ts
                    data.to_sql(name=table, con=engine, if_exists='append', index=True, index_label='timestamp')
                else:
                    data.to_sql(name=table, con=engine, if_exists='append', index=False)

            return True
        else:
            return "You have to pass timestamp indexed dataframe, or dataframe with timestamp present!"

    def load_data_infile(self, fname, table, load_data=True, print_sql=False):
        """
        This method is used to load data from csv files directly.

        This method implements two methods to import csv file to table. If load_data=True, it uses the
        load_data_local_infile method of MySQL to import csv file directly, else
        it reads the file into a DataFrame, and then pushes the DataFrame to the table.

        Args:
            fname (String): The filename to be pushed to table
            table (String): The table into which data to be inserted
            load_data (Optional [Bool]): If True, use Load_Data_Local_Infile command of MySQL to push data, else push data after reading it to DataFrame
            print_sql (Optional [Bool]): If True, print SQL statement, else dont.

        Raises:
            ValueError: When file doesn't exist/ or non csv file passed/ Data import unsuccessful.
            AttributeError: When not a valid filename is passed

        Returns:
            None
        """

        load_file = False

        engine = self.sql_alchemy()
        con = engine.connect()

        if type(fname) is str:
            if fname.lower.endswith('.csv'):
                if os.path.isfile(fname):
                    load_file = True
                else:
                    raise ValueError ('The file doesnt exist. Kindly check again.')
            else:
                raise ValueError('Currently supports only loading data from .csv file. Passed value is {}'.format(fname))
        else:
            raise AttributeError('Wrong type of file name passed. Kindly pass string with .csv ending. Passed value is {}'.format(fname))

        if load_file:
            if self.check_table(table):
                # Check if table exists or not. If it does, pass
                pass
            else:
                # Else, create table, then proceed to pushing data
                df = pd.read_csv(fname, index_col=False)
                schema = self.__get_schema_from_df__(df, table)
                self.create_table(schema)

            # Now that table is created, proceed with pushing data into table.
            if load_data:
                # If data to be loaded using load_data_local_infile command
                print("Importing {}".format(fname))
                sql = "LOAD DATA LOCAL INFILE '{}' INTO TABLE `{}` ".format(fname, table)
                sql += """FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES"""
                sql += """SET timestamp = STR_TO_DATE(@timestamp, '%%Y-%%m-%%d %%H:%%i:%%s')"""

                if print_sql:
                    print("Sql is {}".format(sql))

                try:
                    print("Successfully imported {}".format(fname))
                    con.execute(sql)
                except Exception as e:
                    print("Problem importing {}".format(fname))
                    print(traceback.format_exc())
            else:
                # Else, push data by extracting DataFrame, and then pushing the DF
                df = pd.read_csv(fname, index_col=['timestamp'])
                r = self.push_data(df, table)

                if r:
                    print ("Data Import successful")
                elif type(r) is str:
                    raise ValueError(r)


def connect_db(kind, environment='prod', component='engine', package='pymysql', assist=False):
    """
    connect_db method for dbCon class. Backwards compatible with older usages. Can be used to return database object, or classObject of type DBCon class.

    Please note that, as future versions are released, more and more helper methods will be added based of user requirements.
    Also, these helper methods will use SQLalchemy from current version onwards. Usage of PyMySQL & MySQLdb will be depriciated in future versions, and only older versions of code will be able to use it directly.

    Examples:
        When assist is set to False, it returns database object type. Older versions of code don't need to change their codes. Then can then execute their sql queries as::

            from dbCon import connect_db
            db = connect_db('read')
            c = db.cursor()
            c.execute(sql_statement)
            c.fetchall()

        When assist is set to True, it returns classObj type of class DBCon type. Can be used to create tables, load data, execute sql among many helper methods.
        Currently the following helper methods are defined. Will be extended in future according to needs::

            from dbCon import connect_db
            db = connect_db('read', assist=True)
            db.create_table(schema/metadata)
            db.push_data(dataframe, table_name)
            db.execute_sql(sql)
            db.check_table(table_name)
            db.read_table(table_name, col_names {Optional})
            db.load_data_infile(fname, table)

    Args:
        kind (String): in ['read', 'write'] Important while doing queries from etl.
        environment (String): in ['prod', 'dev'] to select corresponding environment & db
        component (String): in ['engine', 'error_etl'] to classify which database to use depending on conditions.
        package (String): in ['pymysql', 'mysqldb', 'sqlalchemy'] to specify which module to use for queries.
        assist (Bool): If True, helper methods are available to be used, else you need to execute each sql.

    Returns:
        obj (Object): Either database object, if assist=False, else classObject of type DBCon.
    """

    if assist is True:
        # When assist is set to True, and user by mistake passes default package value, switch it to use SQLalchemy.
        package = 'sqlalchemy'

    obj = DBCon(kind=kind, environment=environment, component=component, package=package)

    if assist:
        return obj
    else:
        return obj.connect_db()


def main():
    import numpy as np

    schema = "CREATE TABLE IF NOT EXISTS `deb_test_tbl` ( `timestamp` datetime DEFAULT NULL UNIQUE , `ma100mlvnav001_cpu-MAD_Score` float DEFAULT NULL, " + \
               "`ma100mlvnav001_cpu_FFT_Score` float DEFAULT NULL, `ma100mlvnav001_cpu_LOF_Score` float DEFAULT NULL, `ma100mlvnav001_cpu_actual` float DEFAULT NULL," + \
                "`ma100mlvnav001_cpu_diff` float DEFAULT NULL,  `ma100mlvnav002_cpu-MAD_Score` float DEFAULT NULL, `ma100mlvnav002_cpu_FFT_Score` float DEFAULT NULL" + \
                 ",PRIMARY KEY (`timestamp`)) ENGINE=InnoDB DEFAULT CHARSET=latin1"

    obj = DBCon(kind='read', package='sqlalchemy')
    obj.create_table(schema)

    arr = np.array((1,2,3,4,5,6,7,8,9))
    df = pd.DataFrame(data=arr, columns=['a'])
    result = obj.push_data(df, table='deb_test_tbl', ts_index=False)

    if type(result) is bool:
        if result:
            print ("Successfully loaded data")
        else:
            print ("Failed")
    elif type(result) is str:
        raise ValueError(result)


if __name__ == '__main__':
    main()
