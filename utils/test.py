from sklearn import datasets, svm
from sklearn.linear_model import LogisticRegression
import pandas as pd
from sklearn import cross_validation
from sklearn.grid_search import GridSearchCV
import numpy as np

def main():

    digits = datasets.load_iris()
    X_digits = digits.data
    y_digits = digits.target
    lr = LogisticRegression()
    lr.fit(X_digits, y_digits)
    # print(y_digits)

    # k_fold = cross_validation.KFold(n=6, n_folds=3)
    # for train_indices, test_indices in k_fold:
    #       print('Train: %s | test: %s' % (train_indices, test_indices))

    Cs = np.logspace(-6, -1, 10)

    clf = GridSearchCV(estimator=lr, param_grid=dict(C=Cs))
    clf.fit(X_digits, y_digits)

    ret = cross_val_model(X_digits, y_digits, lr)


def cross_val_model(train, target, model, nfolds=4):
    # print(len(train[0]), len(train))
    # folds = cross_validation.KFold(train.shape[0], n_folds=nfolds, shuffle=False, random_state=np.random.randint(1e4))
    Cs = np.logspace(-6, -1, 10)
    clf = GridSearchCV(estimator=model, param_grid=dict(C=Cs))
    clf.fit(train, target)
    print(clf)
    print(clf.best_params_)
    print(clf.best_score_)

    preds = [
        np.array([model.fit(train[train_indices], target[train_indices]).predict(train[test_indices]),
                  target[train_indices], train_indices])
        for train_indices, test_indices in folds]

    preds = [preds[x].transpose() for x in range(len(preds))]
    # pred_s = [np.concatenate(pred, axis=0) for pred in preds]
    # for pred in pred_s:
    #     print("Data")
    #     print(pred)
    pp = []
    for pred in preds:
        print(len(pred), len(pred[0]))
        print(pred.shape)
        pred = np.reshape(pred, (-1, len(pred)))
        print(pred.shape)
        preds = pd.DataFrame(pred, columns=["pred", "target", "index"])
        pp.append(preds)
    # print(pp)
    # preds = preds.sort(columns="index")
    i = 1
    for p in pp:
        p.to_csv("{}.csv".format(i))
        i += 1
    return pp

if __name__ == '__main__':
    main()