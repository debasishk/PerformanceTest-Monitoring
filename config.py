from AnomalyDetection.Univariate.MedianAbsoluteDeviation import MedianAbsoluteDeviation as MAD
from AnomalyDetection.Univariate.LOF import LOF as LOF
from AnomalyDetection.Univariate.FastFourierTransform import FastFourierTransform as FFT

from AnomalyDetection.Multivariate.kmeans import K_Means
from AnomalyDetection.Multivariate.lof import LocalOutlierFactor
from AnomalyDetection.Multivariate.HCiS import HCiS
from AnomalyDetection.Multivariate.ABOD import AngleBasedOD
from AnomalyDetection.Multivariate.lof_v2 import Lof
from AnomalyDetection.Multivariate.odin import Meandist


cols_to_del = ['id', 'server']

metrics_to_ignore = ['disk-read(kb/s)', 'disk-write(kb/s)']

mad_obj = MAD
fft_obj = FFT
lof_obj = LOF

univariate_model_dict = {'MAD': mad_obj,
                         'FFT': fft_obj,
                         'LOF': lof_obj}

multivariate_model_dict = {'LOF_v2_mv': Lof,
                           'KMeans': K_Means,
                           'HCiS': HCiS,
                           'MeanDist': Meandist}

dbParams = {'host': '127.0.0.1', 'user':'root', 'pwd':'', 'db':'performancetesting',
            'uri':'mysql+pymysql://root:@localhost/performancetesting?charset=utf8&local_infile=1'}