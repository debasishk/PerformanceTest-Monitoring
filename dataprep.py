from utils.dbCon import connect_db


class DataPreparation(object):
    def __init__(self):
        self.dataset = dict()
        self.servers = list()

        self.__driver__()
        pass

    def __driver__(self):
        self.con = self.create_db_con()
        self.servers = self.get_server_names()
        self.prepare_data()
        pass

    def create_db_con(self):
        con = connect_db('read', package='sqlalchemy', assist=True)

        host = '127.0.0.1'
        usr = 'root'
        pwd = ''
        db = 'performancetesting'
        uri = 'mysql+pymysql://{}:@localhost/{}?charset=utf8&local_infile=1'.format(usr, db)

        con.set_params(host=host, user=usr, db=db, pwd=pwd, uri=uri)

        return con

    def get_server_names(self):
        sql = "SELECT DISTINCT(`server`) FROM `pt_metrics`"

        servers = self.con.execute_sql(sql)

        return servers['server'].values.tolist()

    def fetch_metrics(self, server):
        sql = "SELECT * FROM `pt_metrics` WHERE `server` = '{}'".format(server)

        metric = self.con.execute_sql(sql)

        return metric

    def prepare_data(self):
        for server in self.servers:
            metric = self.fetch_metrics(server)

            self.dataset[server] = metric
        pass

    def fetch_(self):
        return self.dataset


if __name__ == '__main__':
    obj = DataPreparation()
