from engine import Engine
from dataprep import DataPreparation
import matplotlib.pyplot as plt
from utils.mkdir import mkdir
from config import *

import datetime as dt

smoother = 'HW'
path = "plots_new\\{}\\".format(smoother)
mkdir(path)
score_full = dict()
type_of_algos = 'multi'

if type_of_algos == 'multi':
    smoother_use = False
else:
    smoother_use = True

methods = list()
metrics_to_include = ['cpu', 'memory']


def main():
    smoother_use = False

    if smoother_use:
        engine_obj = Engine(smoother)
        scores = engine_obj.fetch_()
        score_full['smoother'] = scores

    engine_obj = Engine(None, type=type_of_algos)
    scores = engine_obj.fetch_()
    score_full['w/o smoother'] = scores

    # print(scores)

    methods_used = list(score_full.keys())
    global methods
    methods = methods_used

    # model_based_scores = engine_obj.fetch_model_based_score()

    data_obj = DataPreparation()
    data = data_obj.fetch_()

    value_score = dict()

    for server in data.keys():

        if type_of_algos == 'uni':
            value_score = univariate(data, server, value_score)
        # model_based_score_per_server = model_based_scores[server]
        elif type_of_algos == 'multi':
            value_score = multivariate(data, server, value_score)

    # print(value_score)

    prepare_plots(value_score, type_of_algos)


def multivariate(data, server, value_score):
    df_values = data[server]
    # print("score full")
    # print(score_full)
    score_1 = score_full['w/o smoother'][server]
    df_values = df_values[metrics_to_include]

    tmpDF = df_values.join(score_1, lsuffix='_values', rsuffix='_score')

    value_score[server] = tmpDF

    # title = 'Server: {0}. Metric: {1}'.format(server, col)
    # dir = "plots\\{}-{}".format(server, col[:10])
    # plot_model_based_scores(model_based_score_per_server_per_metric, title, dir, df_values)
    return value_score


def univariate(data, server, value_score):
    value_score[server] = dict()
    for col in data[server].columns.tolist():
        if col in cols_to_del:
            pass
        else:
            if col not in metrics_to_ignore:
                # model_based_score_per_server_per_metric = model_based_score_per_server[col]
                # Uni variate plots
                df_values = data[server][[col]]
                # df_scores = scores[server][[col]]

                # Multivariate
                # df_values = data[server]

                score_1 = score_full['w/o smoother'][server][[col]]
                if hasattr(score_full, 'smoother'):
                    score_2 = score_full['smoother'][server][[col]]
                    score_ = score_1.join(score_2, how='right', lsuffix='_without', rsuffix='_with')

                    score_['average_score'] = score_.apply(lambda a: sum(a.values) / len(a.values), axis=1)

                    score_.rename(columns={'average_score': col}, inplace=True)
                    score_.drop([col + '_without', col + '_with'], axis=1, inplace=True)
                else:
                    score_ = score_1

                tmpDF = df_values.join(score_, lsuffix='_values', rsuffix='_score')

                value_score[server][col] = tmpDF

                # title = 'Server: {0}. Metric: {1}'.format(server, col)
                # dir = "plots\\{}-{}".format(server, col[:10])
                # plot_model_based_scores(model_based_score_per_server_per_metric, title, dir, df_values)
    return value_score


def plot_model_based_scores(score_per_model_dict, title, dir, data):
    for k in score_per_model_dict.keys():
        title = title + '. Model: {}'
        dir = dir + '-{}'
        title = title.format(k)
        dir = dir.format(k)

        df = data.join(score_per_model_dict[k], how='right')

        df.plot(title=title)
        plt.savefig(dir)


def prepare_plots(data, type='uni'):
    """
    Prepare plots. Data should be dictionary of dictionary of dataframes

    :param data:
    :return:
    """
    if type == 'uni':
        for server in data.keys():
            for metrics in data[server].keys():
                value_score = data[server][metrics]
                title = 'Server: {}. Metric: {}'.format(server, metrics)
                fname = '{}-{}.png'.format(server, metrics[:10])
                plot(value_score, title, fname)
    elif type == 'multi':
        for server in data.keys():
            value_score = data[server]
            title = 'Server: {}'.format(server)
            fname = '{}.png'.format(server)
            plot(value_score, title, fname)


def plot(data, title=None, fname=None):
    data.plot(title=title)
    dir = path
    # mkdir(dir)
    fname = dir + fname
    plt.savefig(fname)
    pass


if __name__ == '__main__':
    st = dt.datetime.now()
    main()
    print("Total execution time, ", dt.datetime.now()-st)