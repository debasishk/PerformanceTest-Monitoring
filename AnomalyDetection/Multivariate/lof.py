"""
This script is used to test new models before they can be imported to original model ensemblee
"""


from externals.lof.lof import LOF
from dataprep import DataPreparation
import pandas as pd
from multiprocessing import Process, Manager
import matplotlib.pyplot as plt

outliers_fraction = 0.25

# Going to implement Parallel processing in LOF to improve execution times. Before that, let me record timing of each model.


class LocalOutlierFactor(object):
    def __init__(self, min_points=5):
        self.min_pts = min_points

        if __name__ == '__main__':
            self.data_obj = DataPreparation()
            self.__driver__()
        pass

    def __driver__(self):
        self.data = self.data_obj.fetch_()
        # print(self.data)
        scores = self.prepare_model()

        pass

    def apply_(self, data=None):
        self.data = data
        scores = self.prepare_model()

        return scores

    def apply(self, data=None):

        lof_ = LOF(data.values)
        self.lof_ = lof_

        manager =Manager()
        return_val = manager.list()

        lof_score = manager.list()
        jobs = []

        for instance in data.values:
            p = Process(target=self.calculate_lof, kwargs={'minPts': self.min_pts, 'instance': instance, 'return_val': return_val, 'score': lof_score})
            jobs.append(p)
            p.start()
            # print("Multitasking")

        for proc in jobs:
            proc.join()

        data['lof_score'] = lof_score

        return lof_score

    def calculate_lof(self, minPts, instance, return_val, score):
        lof_ = self.lof_.local_outlier_factor(minPts, instance)
        return_val = lof_
        score.append(return_val)

    def prepare_model(self):

        if type(self.data) is dict:
            dict_data = True
        elif type(self.data) is pd.core.frame.DataFrame:
            dict_data = False

        data = self.data
        scores = dict()

        if dict_data:
            for k in data.keys():
                tmpDF = data[k]
                # df = tmpDF[['cpu', 'memory']]
                score = self.apply(tmpDF)
                scores[k] = score
        else:
            tmpDF = self.data
            # df = tmpDF[['cpu', 'memory']]
            scores = self.apply(tmpDF)
        # print("tests & debugs")
        scores = list(scores)
        # print(type(scores), type(scores[0]), scores)
        # print(type(self.data.index.tolist()), type(self.data.index.tolist()[0]), self.data.index.tolist())
        result = pd.DataFrame(data=scores, columns=['LOF_mv_score'], index=self.data.index.tolist())
        # print(result)

        return result


if __name__ == '__main__':
    obj = LOF()