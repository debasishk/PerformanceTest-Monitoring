from sklearn.cluster import KMeans
from dataprep import DataPreparation
import pandas as pd
from scipy import stats
from numpy.linalg import norm
import matplotlib.pyplot as plt


class K_Means(object):
    def __init__(self, outliers_fraction=0.25, n_clusters=2):
        self.outier_fraction = outliers_fraction
        self.n_clusters = n_clusters

        if __name__ == '__main__':
            self.data_obj = DataPreparation()
            self.__driver__()
        pass

    def __driver__(self):
        self.data = self.data_obj.fetch_()
        # print(self.data)
        scores = self.prepare_model()

        pass

    def apply_(self, data=None):
        self.data = data
        scores = self.prepare_model()

        scores = pd.DataFrame(data=scores, index=data.index.tolist(), columns=['KMeans_score'])

        return scores

    def apply(self, data=None):

        k_means_ = KMeans(n_clusters=self.n_clusters)

        ret = k_means_.fit(data.values)
        data['labels'] = ret.labels_

        center_1 = ret.cluster_centers_[0][0]
        center_2 = ret.cluster_centers_[0][1]

        scores = self.calculate_scores_from_centers(data, center_1, center_2)

        data['kmeans_score'] = scores

        return scores

    def calculate_scores_from_centers(self, data, center_1, center_2):
        indeces = data.index.tolist()
        scores = []
        for idx in indeces:
            row = data.ix[idx]
            row = row.values

            dist_1 = norm(row-center_1)
            scores.append(dist_1)

        return scores

    def prepare_model(self):

        if type(self.data) is dict:
            dict_data = True
        elif type(self.data) is pd.core.frame.DataFrame:
            dict_data = False

        data = self.data
        scores = dict()

        if dict_data:
            for k in data.keys():
                tmpDF = data[k]
                df = tmpDF[['cpu', 'memory']]
                score = self.apply(df)
                scores[k] = score
        else:
            tmpDF = self.data
            df = tmpDF[['cpu', 'memory']]
            scores = self.apply(df)

        return scores


if __name__ == '__main__':
    obj = ModelTests()