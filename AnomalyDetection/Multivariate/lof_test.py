"""
This script is used to test new models before they can be imported to original model ensemblee
"""


from sklearn import svm
from lof.lof import LOF, outliers
from dataprep import DataPreparation
import pandas as pd
from scipy import stats
import matplotlib.pyplot as plt

outliers_fraction = 0.25


class ModelTests(object):
    def __init__(self):
        self.data_obj = DataPreparation()
        self.__driver__()
        pass

    def __driver__(self):
        self.data = self.data_obj.fetch_()
        # print(self.data)
        self.prepare_model()
        pass

    def apply_(self, data=None):
        data.plot()
        plt.show()
        # print(data.values)
        lof_ = LOF(data.values)
        lof_score = []

        for instance in data.values:
            lof__ = lof_.local_outlier_factor(5, instance)
            lof_score.append(lof__)

        data['lof_score'] = lof_score
        print(data)

        return data

    def prepare_model(self):
        if type(self.data) is dict:
            dict_data = True
        elif type(self.data) is pd.core.DataFrame:
            dict_data = False

        data = self.data

        for k in data.keys():
            tmpDF = data[k]
            df = tmpDF[['cpu', 'memory']]
            self.apply_(df)


if __name__ == '__main__':
    obj = ModelTests()