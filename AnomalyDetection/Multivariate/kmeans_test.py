"""
This script is used to test new models before they can be imported to original model ensemblee
"""


from sklearn.cluster import KMeans
from dataprep import DataPreparation
import pandas as pd
from scipy import stats
from numpy.linalg import norm
import matplotlib.pyplot as plt

outliers_fraction = 0.25


class ModelTests(object):
    def __init__(self):
        self.data_obj = DataPreparation()
        self.__driver__()
        pass

    def __driver__(self):
        self.data = self.data_obj.fetch_()
        # print(self.data)
        self.prepare_model()
        pass

    def apply_(self, data=None):
        data.plot()
        plt.show()
        # print(data.values)
        k_means_ = KMeans(n_clusters=2)
        kmeans_score = []

        ret = k_means_.fit(data.values)
        data['labels'] = ret.labels_

        center_1 = ret.cluster_centers_[0][0]
        center_2 = ret.cluster_centers_[0][1]

        scores = self.calculate_scores_from_centers(data, center_1, center_2)
        print(scores)
        data['kmeans_score'] = scores
        print(data)

        return data

    def calculate_scores_from_centers(self, data, center_1, center_2):
        indeces = data.index.tolist()
        scores = []
        for idx in indeces:
            row = data.ix[idx]
            row = row.values

            dist_1 = norm(row-center_1)
            scores.append(dist_1)

        return scores

    def prepare_model(self):
        if type(self.data) is dict:
            dict_data = True
        elif type(self.data) is pd.core.DataFrame:
            dict_data = False

        data = self.data

        for k in data.keys():
            tmpDF = data[k]
            df = tmpDF[['cpu', 'memory']]
            self.apply_(df)


if __name__ == '__main__':
    obj = ModelTests()