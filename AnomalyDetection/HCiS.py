# This module implements HCiS-High Contrast Subspaces for Density-Based Outlier Ranking outlier detection


import pandas as pd
import numpy as np
import itertools
import random

from scipy import stats

from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import MinMaxScaler


class LOF(object):
    def __init__(self, k=3, MinPts=50):
        self.k = k
        self.minPts = MinPts
        pass

    def apply_(self, data=pd.DataFrame()):
        self.df = data

        knndist, knnindices = self.knn(data)
        reachdist, reachindices = self.reachDist(data, knndist)
        irdMatrix = self.ird(reachdist)
        lofScores = self.lof(irdMatrix, reachindices)

        return lofScores

    def knn(self, data):
        nbrs = NearestNeighbors(n_neighbors=self.k)
        nbrs.fit(self.df)
        distances, indices = nbrs.kneighbors(self.df)
        return distances, indices

    def reachDist(self, data, knnDist):
        nbrs = NearestNeighbors(n_neighbors=self.minPts)
        nbrs.fit(self.df)
        distancesMinPts, indicesMinPts = nbrs.kneighbors(self.df)
        distancesMinPts[:, 0] = np.amax(distancesMinPts, axis=1)
        distancesMinPts[:, 1] = np.amax(distancesMinPts, axis=1)
        distancesMinPts[:, 2] = np.amax(distancesMinPts, axis=1)
        return distancesMinPts, indicesMinPts

    def ird(self, knnDistMinPts):
        return (self.minPts / np.sum(knnDistMinPts, axis=1))

    def lof(self, Ird, dsts):
        lof = []
        for item in dsts:
            tempIrd = np.divide(Ird[item[1:]], Ird[item[0]])
            lof.append(tempIrd.sum() / self.minPts)
        return lof


class HCiS(object):
    def __init__(self, k=3, MinPts=50):
        self.LOF_obj = LOF(k, MinPts)
        pass

    def apply_(self, data):
        self.df = data
        self.index_df = (data.rank()/data.rank().max()).iloc[:,:-1]

        selection = self.prepare_()

        scoresList = []
        for item in selection:
            da = self.df[item]
            lofScores = self.LOF_obj.apply_(da)
            scoresList.append(lofScores)

        avgs = np.nanmean(np.ma.masked_invalid(np.array(scoresList)), axis=0)

        scaled_avgs = MinMaxScaler().fit_transform(avgs.reshape(-1, 1))

        return scaled_avgs

    def comboGenerator(self, startPoint,space,n):
        combosFinal = []

        for item in itertools.combinations(list(set(space) - set(startPoint)), (n - len(startPoint))):
            combosFinal.append(sorted(startPoint + list(item)))
        return combosFinal

    def prepare_(self):
        listOfCombos = self.comboGenerator([], self.df.columns[:-1], 2)
        testedCombos = []
        selection = []

        while (len(listOfCombos) > 0):
            if listOfCombos[0] not in testedCombos:
                # print "Calculating {0}".format(listOfCombos[0])
                alpha1 = pow(0.2, (float(1) / float(len(listOfCombos[0]))))
                pvalue_Total = 0
                pvalue_cnt = 0
                avg_pvalue = 0
                for i in range(0, 50):
                    lband = random.random()
                    uband = lband + alpha1
                    v = random.randint(0, (len(listOfCombos[0]) - 1))
                    rest = list(set(listOfCombos[0]) - set([listOfCombos[0][v]]))
                    k = stats.ks_2samp(self.df[listOfCombos[0][v]].values,
                                       self.df[((self.index_df[rest] < uband) & (self.index_df[rest] > lband)).all(axis=1)][
                                           listOfCombos[0][v]].values)
                    # print "iter:{4},lband:{0},uband:{1},v:{2},pvalue:{3},length:{5},rest:{6}".format(lband,uband,v,k.pvalue,i,len(df[((index_df[rest]<uband) & (index_df[rest]>lband)).all(axis=1)][listOfCombos[0][v]]),rest)
                    if not (np.isnan(k.pvalue)):
                        pvalue_Total = pvalue_Total + k.pvalue
                        pvalue_cnt = pvalue_cnt + 1
                if pvalue_cnt > 0:
                    avg_pvalue = pvalue_Total / pvalue_cnt
                    # print avg_pvalue
                if (1.0 - avg_pvalue) > 0.75:
                    selection.append(listOfCombos[0])
                    listOfCombos = listOfCombos + self.comboGenerator(listOfCombos[0], self.df.columns[:-1],
                                                                 (len(listOfCombos[0]) + 1))
                testedCombos.append(listOfCombos[0])
                listOfCombos.pop(0)
                listOfCombos = [list(t) for t in set(map(tuple, listOfCombos))]
            else:
                listOfCombos.pop(0)

        return selection
