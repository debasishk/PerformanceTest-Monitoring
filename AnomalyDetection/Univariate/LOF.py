# Project: Predictive Analytics
# Author: Debasish Kanhar
# UserID: BC03421


import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors
import datetime
from matplotlib import pyplot as plt
from random import uniform
import os
from collections import Counter
# from utils.mkdir import mkdir


plt_pth = 'Results\KalmanFilters\All\LOF\\'


class LOF:
    """
    This class is used to calculate outliers in data based on Local Outlier Factor algorithm.
    """
    def __init__(self, neighbours, minPts, threshold, plot_path=plt_pth, forecaster='KalmanFilters', data=pd.DataFrame(), test_data=False, plot=False, vars=1):
        """ web function for Local Outlier Factor Algorithm to initialize parameters

        Args:
            date_start (datetime.datetime): Start date for extraction of data
            date_end (datetime.datetime): End date for extraction of data
            nbrs (int): Number of Neighbours to be kept for each data point
            minPts (int): Minimum number of points to be considered in each point's neighbourhood
            threshold (float): Threshold. LOF scores more than this are flagged as outliers
            plot_path (str): The path name where plots are to be saved
            forecaster (str): The difference/data is calculated based on this forecasting algorithm.
            data (Pandas Dataframe): Input data whose outliers are to be found out
            test_data (bool): Defaults to False. If model to be used for Unit Tests, explicitely specify this variable as True to bypass get_data() method.

        Returns:
            None
        """

        self.k = neighbours
        self.minPts = minPts
        self.threshold = threshold
        self.plot = plot
        self.vars = vars

        self.file = '..//..//..//Datasets//metrics_train_booz4.csv'
        if len(data.index) == 0:
            self.metric = 'navapp_resp_a'
        else:
            cols = data.columns.tolist()
            col = [elem for elem in cols if 'timestamp' not in elem]
            col = col[0]
            self.metric = col

        self.plot_path = plot_path + '{}\\LOF\\'.format(self.metric)

        self.tmpData = data

        # mkdir(self.plot_path + '\\{}\\'.format(self.metric))

        if not test_data:
            if len(data.index):
                self.__driver__()
        pass

    def apply_(self, data=pd.DataFrame()):
        """
        Applies Local Outlier Factor algorithm to input dataset to find out Outliers and return scores of each point.

        Args:
            data (Pandas DataFrame): Input dataset on which outlier scores based on LOF algorithm to be found out.

        Returns:
            scores (Pandas DataFrame): LOF based outlier scores for each point stored in Pandas DataFrame indexed by tinestamp.
        """

        self.tmpData = data

        self.__driver__()

        scores, outliers = self.return_scores_and_outliers()

        return scores

    def __driver__(self):
        """ Driver method for LOF class to calculate outliers
        Returns:
            None
        """

        self.__get_data__()
        self.fit_data()
        self.__plot__()

        pass

    def test(self, data=None, columns=None):
        """
        Test the Local Outlier Factor module. This method is used for unit tests.

        Args:
            data (pandas Dataframe): The data to be tested against.
                                    It can have either '2' columns in DF, which corresponds to respective metrics, and timestamp,
                                    Or '1' column with index as timestamp
            columns (List of strings): The columns on which Local Outlier Factor algorithm to be analyzed.

        Returns:
            test_output (Pandas DataFrame): Pandas Dataframe object containing 1 column which holds scores for each point
        """
        self.tmpData = data

        if not columns:
            columns = data.columns.tolist()

        else:
            columns = columns

        test_output = dict()

        for col in columns:
            if col != 'timestamp':
                self.metric = col
                self.data = data[self.metric].values
                self.data = self.data.reshape(-1, 1)
                self.data = self.data.astype('float64')

                for elem in self.data:
                    elem *= uniform(1.0,1.01)

                self.fit_data()
                self.get_outliers()
                test_output[col + '_LOF_score'] = self.final_data['Score']

        output = pd.DataFrame.from_dict(data=test_output)

        return output

    def __get_data__(self):
        """ Fetches data from passed variables, and does small manipulations so that there are no repetitive values thus avoiding NaNs in scores.

        Selects the metric name based on columns from dataframe passed to LOF class. Reshapes it to size (-1, 1) and then
        multiplies a random number between (1, 1.01) to each number in self.data to avoid repetitive values.

        Returns:
            None, Stores final data in self.data instance variable
        """

        if self.vars == 1:
            self.metric = [col for col in self.tmpData.columns.tolist() if col is not 'timestamp'][0]
            self.data_ts = self.tmpData.index.tolist()
            self.data = self.tmpData[self.metric].values

            self.data = self.data.reshape(-1, 1)
            for elem in self.data:
                elem *= uniform(1.0,1.01)

        else:
            cols = self.tmpData.columns.tolist()
            metric = "_".join(cols)
            self.metric = metric

            self.data = self.tmpData

            self.data.reset_index(inplace=True)
            col_n = self.data.columns.tolist()

            if 'index' in col_n:
                self.data_ts = self.data['index'].values.tolist()
                self.data.set_index('index', inplace=True)
            else:
                for col in col_n:
                    if col in cols:
                        pass
                    else:
                        self.data_ts = self.data.index.tolist()
                        self.data.set_index(col, inplace=True)

    def fit_data(self):
        """
        Wrapper function to call required methods for calculating Outliers.

        Returns:
            None
        """

        self.knn()
        self.reachDist()
        self.lrd()

        self.__lof__()

        pass

    def knn(self):
        """ Calculates the distances and indeces of each point to its neighbouts.

        Applies KNearestNeighbours algorithm to find out nearest neighbours to each data point. Number of neighbours
        is defined by instance variable self.k

        Returns:
            None, stores distances and indecec array in self.distances and self.indeces
        """

        nbrs = NearestNeighbors(n_neighbors=self.k)
        nbrs.fit(self.data)
        distances, indeces = nbrs.kneighbors(self.data)

        self.distances = distances
        self.indeces = indeces

    def reachDist(self):
        """ Calculates reach distance to its nearest points for each point in data point.

        Calculates 'x' nearest points to a particular data point, where 'x' is defined by self.minPts. Finds its distance
        from each nearest point, and indeces of nearest points using NearestNeighbours algorithm.

        Attributes:
            self.knnDistMinPts (numpy array) (n X minPts): Distance of 'minPts' points closest to current data point.
            self.dsts (numpy array) (n X minPts): Indeces of 'minPts' points closest to current data point.

        Returns:
            None
        """

        nbrs = NearestNeighbors(n_neighbors=self.minPts)
        nbrs.fit(self.data)
        distanceMinPts, indecesMinPts = nbrs.kneighbors(self.data)

        distanceMinPts[:,0] = np.amax(distanceMinPts,axis=1)
        # distanceMinPts[:,1] = np.amax(distanceMinPts,axis=1)
        # distanceMinPts[:,2] = np.amax(distanceMinPts,axis=1)

        self.knnDistMinPts = distanceMinPts
        self.dsts = indecesMinPts

    def lrd(self):
        """ Calculates the IRD matrix used in Local Outlier Factor algorithm

        Attributes:
            self.Ird (numpy matrix): Stores the Ird matrix used in calculating Local Outlier score.

        Returns:
            None
        """

        irdMatrix = (self.minPts/np.sum(self.knnDistMinPts, axis=1))
        self.Ird = irdMatrix

    def __lof__(self):
        """ Calculates LOF scores for each data point

        Attributes:
            self.lof (List of Floats): Stores the LOF score of each data point

        Returns:
            None
        """

        lof = []
        for item in self.dsts:
            tmpIrd = np.divide(self.Ird[item[1:]], self.Ird[item[0]])
            lof.append(tmpIrd.sum()/self.minPts)
        self.lof = lof

    def returnFlag(self, x):
        """ Flags a data point as outlier or not depending on its score being >= threshold or not.

        Args:
            x (int or float): The data point as single element

        Returns:
            1 or 0: Flag value. 1 if x is flagged as error else 0
        """

        # Select optimal threshold
        if x['Score'] >= self.threshold:
            return 1
        else:
            return 0
        pass

    def scale_scores(self, x):
        """
        Inbuilt method to scale raw scores in range [1-100]

        Args:
            x (Float): The raw score which needs to be scaled

        Returns:
            score (Float): The scaled score in range 1-100
        """
        score = (100)*((x['unscaled']-min(self.lof))/(max(self.lof)-min(self.lof))) + 1
        return score

    def get_outliers(self):
        """ Get outliers for passed dataset. Creates new columns names 'Score' and 'flag' to store LOF scores and LOF flags (Outlier or not)

        Attributes:
            self.final_data (pandas Dataframe): Stores the final dataset with columns 'timestamp, original value, LOF score and LOF flag'

        Returns:
            None.
        """

        scores = pd.DataFrame(self.lof, columns=['Score'])
        # scores['Score'] = scores.apply(self.scale_scores, axis=1)
        try:
            self.tmpData = self.tmpData.reset_index()
            self.tmpData.drop('index', axis=1, inplace=True)
        except:
            pass

        mergedData = pd.merge(self.tmpData, scores, left_index=True, right_index=True)

        mergedData['flag'] = mergedData.apply(self.returnFlag, axis=1)

        self.final_data = mergedData
        pass

    def return_scores_and_outliers(self):
        """ Returns Scores and Outliers to outside module. Can be called only after object of type LOF class has been initialized.

        This is only function accessible outside LOF class, and is used to return only scores and Outliers in data.

        Returns:
            Score_df (Pandas DataFrame): DataFrame storing LOF based scores for each point
            Outliers (Pandas Dataframe): Dataframe object which stores all the Outliers identifies in dataset, including its original value, and scores
        """

        self.get_outliers()
        Outliers = self.final_data[self.final_data['flag'] == 1]

        score = self.final_data['Score'].values
        score_df = pd.DataFrame(data=score, index=self.data_ts, columns=['{}_LOF_Score'.format(self.metric)])
        return score_df, Outliers

    def __plot__(self):
        """ Plots the results and saves it to Result directory

        Returns:
            None, saves the plots to path specified.
        """

        if len(self.tmpData.index):
            self.get_outliers()
            Outliers = self.final_data[self.final_data['flag'] == 1]
            Normals = self.final_data[self.final_data['flag'] == 0]

            if self.plot:
                plt.clf()
                plt.plot(self.tmpData[self.metric].values.tolist())
                plt.scatter(Outliers.index.tolist(), Outliers[self.metric].values.tolist())
                plt.savefig(self.plot_path + 'outliers.png')
        else:
            scores = pd.DataFrame(self.lof,columns=['Score'])
            self.DF = self.DF.reset_index()
            self.DF.drop('index', axis=1, inplace=True)

            mergedData = pd.merge(self.DF, scores, left_index=True, right_index=True)

            mergedData['flag'] = mergedData.apply(self.returnFlag, axis=1)

            Outliers = mergedData[mergedData['flag'] == 1]
            Normals = mergedData[mergedData['flag'] == 0]

            if self.plot:
                plt.plot(self.data)
                plt.scatter(Outliers.index.tolist(), Outliers[self.metric].values.tolist())
                plt.savefig(self.plot_path + 'outliers.png')
        pass


if __name__ == '__main__':
    date_start = datetime.datetime(2015, 11, 28)
    date_end = datetime.datetime(2015, 11, 29)

    obj = LOF
    obj(date_start, date_end, 3, 15, 1.2)