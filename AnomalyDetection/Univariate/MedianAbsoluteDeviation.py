# Project: Predictive Analytics
# Author: Debasish Kanhar
# UserID: BC03421

# This module implements class known as Median Absolute Deviation which selects outliers based on Median absolute Deviation algorithm.

import numpy as np
import matplotlib.pyplot as plt
import datetime, os
import pandas as pd
# from utils.mkdir import mkdir


plt_pth = 'Results\KalmanFilters\All\LOF\\'


class MedianAbsoluteDeviation:
    """
    MedianAbsoluteDeviation calculates outlliers in data based on deviation from Median instead of Standard Deviation which is weak algorithm
    """
    def __init__(self, threshold, forecaster='KalmanFilters', out_path=plt_pth,
                 data=pd.DataFrame(), difference=True, test_data=False, plot=False):
        """ web method for MedianAbsoluteDeviation class.
        Args:
            threshold (float): The threshold to identify outliers. z_score >= threshold are identified as Outliers
            difference (bool): Defaults to True. If True, then difference of Actual and Predicted values to be passed for outlier detection
            forecaster (str): Forecasting algorithm used to predict vaues
            out_path (str): Path where results are saved
            data (Pandas Dataframe): The input data in which Outliers are to be found
            test_data (bool): Defaults to False. If model to be used for Unit Tests, explicitely specify this variable as True to bypass get_data() method.
            plot (bool, Optional [Default False]): Flag variable to mention weather to plot results & save it or not.

        Returns:
            None
        """

        self.data = data
        self.threshold = threshold
        self.diff = difference
        self.forecaster = forecaster
        self.plot = plot

        self.fmt = '%Y-%m-%d %H:%M:%S'
        self.actual_col = 'actual'
        self.prediction_col = 'prediction'

        if hasattr(self, 'metric'):
            self.plot_dir = out_path + '{}/MAD/thresh-{}/complete//'.format(self.metric, threshold)
        else:
            self.plot_dir = out_path + 'plot/Outliers/{}/MAD/thresh-{}/complete//'.format(forecaster, threshold)

        # mkdir(self.plot_dir)

        if not test_data:
            if len(data.index):
                self.__driver__()
        pass

    def apply_(self, data=pd.DataFrame()):
        """
        Applies Median Absolute Deviation algorithm to input dataset to find out Outliers and return scores of each point.

        Args:
            data (Pandas DataFrame): Input dataset on which outlier scores based on MAD algorithm to be found out.

        Returns:
            scores (Pandas DataFrame): MAD based outlier scores for each point stored in Pandas DataFrame indexed by tinestamp.
        """

        self.data = data

        self.__driver__()

        scores, outliers = self.return_scores_and_outliers()

        return scores

    def __driver__(self):
        """ Driver method for MedianAbsolluteDeviation class
        Returns:
            None
        """

        self.__get_data__()
        self.MAD()
        pass

    def test(self, data=None, columns=None):
        """
        Test the Median Absolute Deviation module. This method is used for unit tests.
        Args:
            data (pandas Dataframe): The data to be tested against.
                                    It can have either '2' columns in DF, which corresponds to respective metrics, and timestamp,
                                    Or '1' column with index as timestamp

        Returns:
            test_output (Pandas DataFrame): Pandas Dataframe object containing 1 column which holds scores for each point
        """

        flag = 0
        for col in data.columns.tolist():
            if col == 'timestamp':
                flag = 1

        if flag == 1:
            self.data_ts = data['timestamp'].values
        else:
            self.data_ts = data.index.tolist()

        if not columns:
            columns = data.columns.tolist()

        else:
            columns = columns

        score = dict()

        for col in columns:
            self.metric = col
            self.data = data[self.metric].values
            self.MAD()

            score[col + '_MAD_score'] = self.scores

        tmpList = []
        for k in score.keys():
            tmp = score[k]
            tmp.reset_index(inplace=True)
            tmpList.append(tmp)

        output = reduce(lambda left, right: pd.merge(left, right, on='index'), tmpList)
        output = output.drop('index', axis=1)

        return output

    def __convert_timestamps__(self):
        """ Converts timestamps to datetime.datetime from strings defined by format in self.fmt instance.

        Returns:
            None, Creates new dataframe object self.data with new timestamp column on dtype datetime.datetime.
        """

        ts = self.data['timestamp'].tolist()
        ts = [datetime.datetime.strptime(elem, self.fmt) for elem in ts]

        self.data.drop('timestamp', axis=1)
        self.data['timestamp'] = ts
        self.data.set_index(['timestamp'])
        pass

    def __get_data__(self):
        """ Converts input data of type Pandas Dataframe object to numpy array

        Attributes:
            self.data (numpy.array): The data on which Outliers to be found out.
        Returns:
            None, stores final extracted data in instance variable.
        """

        self.metric = [col for col in self.data.columns.tolist() if col is not 'timestamp'][0]

        if len(self.data.index):
            self.data_ts = self.data.index.tolist()
            self.data = self.data[self.metric].values

        else:
            raise ValueError('Empty Pandas dataframe passed as input data. Kindly check your input data')
        pass

    def __create_dirs__(self):
        """ Create directory to save all results. (CSV files and Plots). Modifies path name according to platform being used

        Returns:
            None
        """
        if not os.path.exists(self.plot_dir):
            os.makedirs(self.plot_dir)
        pass

    def MAD(self):
        """ Calculates outliers based on MedianAbsoluteDeviation algorithm

        Attributes:
            self.outliers (Pandas Dataframe): Pandas dataframe storing only the Outliers identified in dataset.
            self.scores (numpy.array, dtype float): Scores for each point which is used to detect weather a point is outlier or not.

        Returns:
            None.
        """

        l = 0
        try:
            l = len(self.data.index)
        except AttributeError:
            l = len(self.data)

        if l:
            try:
                data = self.data[self.metric].values
            except IndexError:
                data = self.data
            pass
        else:
            self.__get_data__()
            data = self.df

        if len(data.shape) == 1:
            data = data[:,None]

        median = np.median(data, axis=0)
        diff = np.sum((data-median)**2, axis=-1)
        diff = diff.astype(np.float64)
        diff = np.sqrt(diff)

        # Replace 0s with 1e-5 * median to avoid NaNs in scores
        diff[diff == 0] = 1e-5 * median

        med_abs_deviation = np.median(diff)
        modified_z_score = 0.6745*diff/med_abs_deviation

        mask = modified_z_score > self.threshold

        actuals = self.data.tolist()

        outlier_df = self.data[mask]

        # print outlier_df
        outlier_actual = outlier_df.tolist()

        if self.plot:
            ts_actual = [i for i in range(len(self.data))]
            # ts_actual = self.data.index.tolist()

            outlier_ts = outlier_df.index.tolist()

            fig = plt.figure()
            plt.plot(ts_actual, actuals)
            plt.scatter(outlier_ts, outlier_actual)
            fig.savefig(self.plot_dir + 'median_absolute_deviation thresh{}.png'.format(self.threshold))

        if not len(self.data):
            outlier_df.drop(self.prediction_col, inplace=True, axis=1)

        self.outliers = pd.DataFrame(data=outlier_df, columns=['outliers'])

        score_df = pd.DataFrame(data=modified_z_score, index=self.data_ts, columns=['{}_MAD_Score'.format(self.metric)])
        self.scores = score_df

        pass

    def return_scores_and_outliers(self):
        """ Returns Outliers and Scores of each data point in dataset. Only function accessible from outside the score of class.

        Returns:
            self.outliers (Pandas Dataframe): Pandas dataframe storing only the Outliers identified in dataset.
            self.scores (Pandas DataFrame, dtype float): Scores for each point which is used to detect weather a point is outlier or not.
        """

        return self.scores, self.outliers


def main():
    # Input data list is of form numpy array

    difference = True
    date_start = datetime.datetime(2015, 11, 20)
    date_end = datetime.datetime(2015, 11, 21)
    threshold = 9

    obj = MedianAbsoluteDeviation
    obj(threshold, difference, date_start, date_end)
#
# if __name__ == '__main__':
#     main()
