# Predictive-Analytics
# Debasish Kanhar
# BC03421
# 06-06-2016

"""
This module is used to calculate correlation between all tuples. The output is in following format:
    Dictionary: { 'server_name_1': { 'server_name_2': { 'pearsonr': pearsonr_correlation,
                                                        'normal': Normal correlation method },
                                     'server_name_3': { 'pearsonr': pearsonr_correlation,
                                                        'normal': Normal correlation method },
                                     .. so on for all remaining servers. }
                  'server_name_2': { 'server_name_1': { 'pearsonr': pearsonr_correlation,
                                                        'normal': Normal correlation method },
                                     'server_name_3': { 'pearsonr': pearsonr_correlation,
                                                        'normal': Normal correlation method },
                                     .. so on for all remaining servers. }
                  .. so on for all remaining servers. }
"""

from numpy.core.multiarray import correlate
from scipy.stats import pearsonr
import numpy as np


class Correlate(object):
    def __init__(self):
        self.correlation_type_1 = 'pearsonr'
        self.correlation_type_2 = 'normal'
        self.input_key = 'tuple'                # Should be in ['tuple', 'last_data']
        pass

    def get_server_names(self, data):
        servers = data.keys()
        return servers

    def split_data_by_server_to_tuple(self, data):
        dataset = dict()

        for server in self.servers:
            dataset[server] = data[server][self.input_key]

        return dataset

    def get_pearsonr(self, arr1, arr2):
        try:
            pearson = pearsonr(arr1, arr2)
        except TypeError:
            pearson = [np.nan, 1]

        return pearson

    def get_correlation(self, arr1, arr2):
        return correlate(arr1, arr2)

    def prepare_correlation_matrix(self, dataset):

        corr_mat = dict()

        for server_1 in self.servers:

            corr_mat[server_1] = dict()

            for server_2 in self.servers:
                if server_1 != server_2:
                    corr_mat[server_1][server_2] = dict()

                    data_for_server_1 = dataset[server_1]
                    data_for_server_2 = dataset[server_2]

                    corr_mat[server_1][server_2][self.correlation_type_1] = \
                                                            self.get_pearsonr(data_for_server_1, data_for_server_2)[0]
                    corr_mat[server_1][server_2][self.correlation_type_2] = \
                                                            self.get_correlation(data_for_server_1, data_for_server_2)[0]

        return corr_mat

    def get_correlation_matrix(self, data):
        if type(data) is dict:
            correct_input = True
        else:
            correct_input = False

        if correct_input:
            self.servers = self.get_server_names(data)

            dataset = self.split_data_by_server_to_tuple(data)

            corr_mat = self.prepare_correlation_matrix(dataset)

            return corr_mat

        else:
            raise ValueError ('Input dataset format not correct. Kindly check docstring in DataPreparation module to \
                                have idea what kind of input data to be passed (Output of DataPreparation)')
