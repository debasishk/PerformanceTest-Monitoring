# Predictive-Analytics
# Debasish Kanhar
# BC03421
# 06-06-2016

"""
This sub module is used to prepare data to be used in Peer Analysys. It shall return a dictionary storing dataframes.

Data is arranged such that resultant return-set if of following format:
    Dictionary : { 'server_name_1': { 'last_data': Last timestamp's data as Pandas Series,
                                    'history': Last 1 hour's data as Pandas DataFrame,
                                    'tuple': Last timestamp's data as Tuple. } ,

                    'server_name_2': { 'last_data': Last timestamp's data as Pandas Series,
                                    'history': Last 1 hour's data as Pandas DataFrame,
                                    'tuple': Last timestamp's data as Tuple. } ,
                    ..... }
"""

from utils import connect_db
import pandas as pd
import datetime as dt


class DataPreparation(object):
    def __init__(self):
        self.metrics_tbl = 'matt3_rtp_mcom_navapp_jboss'
        self.score_tbl = ''
        self.col_field_in_show_sql_result = "Field"
        self.prev_hours_data_to_be_fetched = 1

        # Considering all servers have 'cpu' metric. It can be any metric out of 5 metrics, provided dataset is full.
        self.metric = 'cpu'

        self.__driver__()
        pass

    def __driver__(self):
        self.db = self.create_db_obj()
        self.metrics_data = self.fetch_data(self.metrics_tbl)
        self.columns, self.servers = self.get_server_and_column_names()
        self.dataset = self.prepare_data()
        pass

    def create_db_obj(self):
        db = connect_db('read', package='sqlalchemy', assist=True)
        return db

    def fetch_data(self, tbl):
        # This method can be optimized later to fetch only data for time period required, and not whole table.
        # sql = "SELECT * FROM {} ORDER BY TIMESTAMP DESC LIMIT 30".format(tbl)
        # data = self.db.execute_sql(sql)
        data = self.db.read_table(tbl=tbl, cols_to_order_by={'timestamp': 'DESC'})
        return data

    def get_server_and_column_names(self):
        sql = "SHOW COLUMNS FROM {}".format(self.metrics_tbl)
        columns = self.db.execute_sql(sql)
        columns = columns[self.col_field_in_show_sql_result].tolist()

        servers = [elem for elem in columns if self.metric in elem]
        servers = [elem.split("_")[0] for elem in servers]
        return columns, servers

    def get_recent_timestamp_from_data(self, data):
        idx_list = data.index.tolist()
        idx_list = [pd.to_datetime(elem) for elem in idx_list]

        if type(idx_list[0]) is pd.tslib.Timestamp:
            datetime_idx = True
        else:
            datetime_idx = False

        if datetime_idx:
            dt_1 = idx_list[0]
            dt_2 = idx_list[1]

            if dt_1 > dt_2:
                desc_data = True
            else:
                desc_data = False

            if desc_data:
                last_ts = idx_list[0]
            else:
                last_ts = idx_list[-1]

            return last_ts

        else:
            raise ValueError ("Please pass data with datetime index. Other indexed data is not supported")

    def format_data(self, data):
        last_ts = self.get_recent_timestamp_from_data(data)
        ts_before_1hr = last_ts - dt.timedelta(hours=self.prev_hours_data_to_be_fetched)

        idx_list = data.index.tolist()

        last_data = data.ix[last_ts]
        prev_idx = idx_list.index(ts_before_1hr)
        curr_idx = idx_list.index(last_ts)
        historical_data = data.iloc[curr_idx+1:prev_idx]

        dataset = dict()
        dataset['current'] = last_data
        dataset['historical'] = historical_data
        dataset['tuple'] = tuple(last_data.tolist())
        return dataset

    def prepare_data(self):
        data_per_server = dict()

        for server in self.servers:
            cols_to_extract = []
            for col in self.columns:
                if server in col:
                    cols_to_extract.append(col)

            data_per_server[server] = self.metrics_data[cols_to_extract]

        final_dataset = dict()

        for k in data_per_server.keys():
            final_dataset[k] = self.format_data(data_per_server[k])

        return final_dataset

    def fetch_dataset(self):
        return self.dataset
