# Predictive-Analytics
# Debasish Kanhar
# BC03421
# 06-06-2016


"""
This is Engine for initializing Peer Analysis. It shall contain following sub-modules:

    DataPreparation: Sub-module to prepare data for analysis. It fetches all metrics per server across same timestamp, and previous 1hour timestamps also (As backup).
    InitializeParams: Sub-module to initialize parameters to get best results.
    Correlations: Get correlation between metric tuples for all servers.
    PeerAnalysis: The core of module, which calculates outliers on all correlation metrics for servers.
    OutlierDetectionModules: These are the models/algorithms used in OD Engine, to be used to find outlier scores on each merrics_per_server tuples.

"""

from AnomalyDetection.Utils.PeerAnalysis import DataPreparation
from AnomalyDetection.Utils.PeerAnalysis import Correlate
from AnomalyDetection.Utils.PeerAnalysis.PeerAnalysis import PeerAnalysis


class Engine(object):
    def __init__(self):
        self.__driver__()
        pass

    def __driver__(self):
        data_obj = DataPreparation()
        dataset = data_obj.fetch_dataset()

        corr_obj = Correlate()
        corr_mat = corr_obj.get_correlation_matrix(dataset)

        peer_obj = PeerAnalysis()
        result = peer_obj.apply_(corr_mat)

        pass


if __name__ == '__main__':
    obj = Engine()
