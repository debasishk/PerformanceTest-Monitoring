# Project: Predictive Analytics
# Author: Debasish Kanhar
# UserID: BC03421


class InitializeParams(object):
    def __init__(self, forecaster = 'None', typeOfMetric = ''):
        """
        Default constructor for InitializeParams class. Initializes all parameters as attributes.

        Args:
            forecaster (str): The type of forecaster to be used. Accordingly result dir & tables are created.
            typeOfMetric (str): The type of metric to be fetched for analysis

        Attributes:
            self.file (String): File name where dataset is present
            self.table (String): The table name from where data to be fetched.
            self.out_path (String): The output path where results to be saved.
            self.metric (String): The metric name which is being analyzed.
            self.csv_in (Bool): Defaults to False. If True, dataset is read in from csv file instead of PA DB
            self.csv_out (Bool): Defaults to True. If False, no csv file of results in saved.
            self.zip_out (Bool): Defaults to False. If True, the result directory is zipped in end to save memory.
        """

        # Hard Coded paths

        self.file = '..//..//..//Datasets//metrics_train_booz4.csv'
        self.table = 'rtp_mcom_navapp_metrics'

        if forecaster == 'KalmanFilters':
            self.out_tbl = 'deb1_test_KF_{}_anomaly_scores'
        elif forecaster == 'ARIMA':
            self.out_tbl = 'deb1_test_ARIMA_{}_anomaly_scores'
        elif forecaster == 'DoubleSmoothing':
            self.out_tbl = 'deb1_test_HW_{}_anomaly_scores'

        self.out_path = 'Results\\{}\\{}\\'.format(forecaster, typeOfMetric)
        self.metric = 'navapp_resp_a'

        # ----------------------------------------------------------------------------------------------------------- #
        # Flag variables

        self.csv_in = False
        self.csv_out = True
        self.zip_out = False

        # DB URLs
        self.prod_db_url = 'mysql+mysqldb://pa:Pr3dict!v@@esu4v401/predictiveAnalytics'
        pass


class ModelParams(object):
        # Fast Fourier Transform Params
    def __init__(self):
        """
        Initializes all model parameters for various models to be used by Outlier Detection module

        Attributes:
            self.FTT_params (Dictionary): Parameters for FFT algorithm
            self.MAD_params (Dictionary): Parameters for MAD algorithm
            self.LOF_params (Dictionary): Parameters for LOF algorithm
            self.ARIMA_params (Dictionary): Deprecated. Parameters for ARIMA smoothing.
            self.Kalman_params (Dictionary): Parameters for Kalman Filters

        Returns:
            None, Saves all parameters in its instance variables which can be accessed from other modules.
        """

        self.FTT_params = {'thresh_freq': 1,
                           'freq_amp': 1,
                           'window': 5}
        # ----------------------------------------------------------------------------------------------------------- #

        # Median Absolute Deviation params
        self.MAD_params = {'threshold': 5,
                           'difference': True}
        # ----------------------------------------------------------------------------------------------------------- #

        # LOF params
        self.LOF_params = {'threshold': 1.1,
                           'neighbours': 5,
                           'minPts': 15}
        # ----------------------------------------------------------------------------------------------------------- #

        # ARIMA params
        self.ARIMA_params = {'data_diff': 1,
                             'transformation': 'natural_log'}
        # ----------------------------------------------------------------------------------------------------------- #

        # Kalman Filter params
        self.Kalman_params = {'window': 50,
                              'prediction_type': 'smooth',
                              'optimize': True}

        # -------------------------------- Store params in dictionary -------------------------------------------#

        self.model_params = dict()
        self.model_params['LOF'] = self.LOF_params
        self.model_params['MAD'] = self.MAD_params
        self.model_params['FFT'] = self.FTT_params

        self.model_params['KF'] = self.Kalman_params
        self.model_params['HW'] = {'lookback': 10, 'num_steps': 1}
        self.model_params['ARIMA'] = self.ARIMA_params

        self.model_params['LOF_mv'] = {'min_points': 5}
        self.model_params['KMeans'] = {'outliers_fraction':0.25, 'n_clusters':2}
        self.model_params['HCiS'] = {'k':3, 'MinPts':50}
        self.model_params['ABOD'] = {'N':200, 'kNN':100, 'topK':50, 'implementation':'fast'}
        self.model_params['LOF_v2_mv'] = {'k': 10}
        self.model_params['MeanDist'] = {'k': 2, 't':0.1}

        pass