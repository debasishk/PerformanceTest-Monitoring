# Project: Predictive Analytics
# Author: Debasish Kanhar
# UserID: BC03421

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import pearsonr
from numpy.random import random_sample as random


class CalculateWeights(object):
    """
    Class to calculate weight for each model by evaluation their various rates & correlation between models.
    """
    def __init__(self):
        """
        Default constructor for CalculateWeights class.

        Attributes:
            self.m_score (String): The model's name whose score to be weighted in comparison to others.
            self.models (List of String): The model's name with respect to whom scores to be weighted.
            self.metrics (List of String): The metrics for whoich scores to be weighted.
            self.correlation (Dictionary): Stores correlation between various model's scores.
            self.correlation_weights (Dictionary): Stores weights assigned to various correlation between model's
            self.strings (List of String): The factors to be considered to weigh model. Currently used TP, FP, FN and correlation.
        """
        self.m_score = 'MAD'
        self.models = ['FFT', 'MAD', 'LOF', 'MF', 'CH', 'DB']
        self.metrics = ['transactions', 'memory', 'gc', 'cpu', 'respTime']
        self.correlation = dict()
        self.correlation_weights = dict()
        self.equations = dict()
        self.weights = dict()
        self.strings = ['tpr', 'fpr', 'fnr', 'correlation']

        pass

    def fit(self, df=pd.DataFrame()):
        """
        Fit method for calculate_weights API. Simply call this method after instance variables are initialized to calculate weights.

        For each pair of metric-server pairs, fetch data/scores. Once socres are fetched, fetch actual_anomalies data.
        Once both are fetched, pass that to evaluation API to get TP/FP/FN and other factors.
        Then, initialize class attributes TP/FP/TN to ones returned. Followed by that, calculate weights

        Args:
            df (Pandas DataFrame): The input DataFrame from which scores to be weighted. It should have timestamp index,
                                    and 7 columns corresponding to each server, and each server will have 6X{model}_score & 1X{server}_metric.

        Returns:
            new_df (Pandas DataFrame): New DataFrame object with weighted scores in it.
        """

        for model in self.models:
            self.m_score = model

            for string in self.strings:
                self.__frame_equations__(string)

        new_df = self.calculate_new_scores(df)

        return new_df

    def __set_tpr__(self, value=0):
        """
        Get true positive rate and store it as Attribute. If value is passed, then store that value, else generate random value for testing purpose.

        Attributes:
            self.tpr (Float): The True positive rate of model.

        Args:
            value (Float): The value to be stores as True Positive Rate
        """
        if value != 0:
            self.tpr = value
        else:
            self.tpr = random()

    def __set_fpr__(self, value=0):
        """
        Get false positive rate and store it as Attribute. If value is passed, then store that value, else generate random value for testing purpose.

        Attributes:
            self.fpr (Float): The False positive rate of model.

        Args:
            value (Float): The value to be stores as False Positive Rate
        """
        if value != 0:
            self.fpr = value
        else:
            self.fpr = random()

    def __set_fnr__(self, value=0):
        """
        Get false negative rate and store it as Attribute. If value is passed, then store that value, else generate random value for testing purpose.

        Attributes:
            self.fnr (Float): The False Negative rate of model.

        Args:
            value (Float): The value to be stores as False Negative Rate
        """
        if value != 0:
            self.fnr = value
        else:
            self.fnr = random()

    def __frame_equations__(self, string):
        """
        This method is used to frame equations from data points and stores the coefficient back in dictionary item.

        Args:
            string (Str): To know which equation to calculate. Either ['tpr', 'fpr', 'fnr', 'tnr']
        """

        self.equations[string] = dict()
        if string == 'tpr':
            self.equations[string]['x'] = [1, 0.75, 0.5, 0.25, 0.01]
            self.equations[string]['y'] = [1, 0.6, 0.3, 0.1, 0]
        elif string == 'fpr':
            self.equations[string]['x'] = [0, 0.25, 0.5, 0.75, 1]
            self.equations[string]['y'] = [1, 0.6, 0.3, 0.1, 0]
        elif string == 'fnr':
            self.equations[string]['x'] = [0, 0.05, 0.15, 0.4,0.5,0.75, 1]
            self.equations[string]['y'] = [1, 0.8, 0.6, 0.25, 0.15, 0.05, 0]
        elif string == 'correlation':
            self.equations[string]['x'] = [0, 0.25, 0.5, 0.75, 1]
            self.equations[string]['y'] = [1, 0.6, 0.3, 0.1, 0]
        else:
            raise ValueError("Only equations for tpr/fpr/fnr/correlation defined. Please pass among those. "
                             "Passed values is {}".format(string))

        self.set_weights(string)

    def calculate_new_scores(self, df=pd.DataFrame()):
        """
        This is final method to calculate the final weighted scores.

        Args:
            df (Pandas DataFrame): Stores metrics, and its each model scores with column name as {model}_score and indexed by timestamp.

        Returns:
            df (Pandas DataFrame): New DataFrame with new columns namely "weighted_{model}_score" added
        """

        for model in self.models:
            new_col = "weighted_{}_score".format(model)
            self.m_score = model
            act_score = df["{}_score".format(model)]
            wt_score = act_score

            for sub_model in self.models:
                if sub_model != model:
                    wt_score *= self.weights[model]['correlation'][sub_model]

            for string in ['tpr', 'fpr', 'fnr']:
                wt_score = wt_score*self.weights[string]

            df[new_col] = wt_score

        return df

    def set_weights(self, string):
        """
        Method to set the calculated weight to corresponding key.

        Attributes:
            self.weights (Dict): Dictionary of dictionary which stores weights on key pairs. Root level keys
                                are model's name and child level keys is among 'tpr', 'fpr', 'fnr', 'correlation'
        Args:
            string (str): In ['tpr', 'fpr', 'fnr', 'correlation'] to define which weight is calculated and hence assign key.
        """

        self.weights[string] = dict()

        method_name = "get_{}_weight".format(string)
        method = getattr(self, method_name)

        x = self.equations[string]['x']
        y = self.equations[string]['y']
        weight = method(x, y)

        self.weights[string][self.m_score] = weight
        pass

    def __get_correlation__(self, df=pd.DataFrame()):
        """
        Calculates the correlation between various scores.

        Args:
            df (Pandas DataFrame): Pandas DF object, with timestamp index. It also needs to ve columns with
                            {server_metric}, {model}_score where model is each of 6 models we use in Anomaly Detection.

        Returns:

        """
        self.correlation[self.m_score] = dict()
        for model in self.models:
            if len(df.index):
                self.correlation[self.m_score]["{}-{}".format(self.m_score, model)] = \
                                            pearsonr(df['{}_score'.format(self.m_score)], df['{}_score'.format(model)])
            else:
                self.correlation[self.m_score]["{}-{}".format(self.m_score, model)] = random()

    def exp_func(self, x, a, b, c):
        """
        Mathematical exponential function which needs to be minimized on set of x-y points passed.

        Function::

            y = a * e^(-b*x) + c

        Args:
            x (Float): The x value for whose y to be calculated depending on exponential function
            a (Float): 'a' intercept
            b (Float): 'b' intercept
            c (Float): 'c' intercept

        Returns:
            y (Float): The final value of function for x
        """

        y = a * np.exp(-b * x) + c
        return y

    def double_exp_func(self, x, a, b, c):
        """
        Mathematical double exponential function which needs to be minimized on set of x-y points passed.

        Function::

            y = a * e^(-2*b*x) + c

        Args:
            x (Float): The x value for whose y to be calculated depending on exponential function
            a (Float): 'a' intercept
            b (Float): 'b' intercept
            c (Float): 'c' intercept

        Returns:
            y (Float): The final value of function for x
        """

        y = a * np.exp(-b * x) + c
        return y

    def pos_exp_func(self, x, a,b, c):
        """
        Mathematical positive exponential function which needs to be minimized on set of x-y points passed.

        Function::

            y = a * e^(b*x) + c

        Args:
            x (Float): The x value for whose y to be calculated depending on exponential function
            a (Float): 'a' intercept
            b (Float): 'b' intercept
            c (Float): 'c' intercept

        Returns:
            y (Float): The final value of function for x
        """

        y = a * np.exp(b * x) + c
        return y

    def get_tpr_weight(self, x, y):
        """
        Calculate tpr weight, for a given true positive rate stored as instance variable

        Args:
            x (List of floats): The x points on plot to calculate the equation to fit on
            y (List of floats): The corresponding y values for x-values given

        Returns:
            tpr_weight (Float): The weight assignet to that particular true positive rate.
        """

        coeffs, b = curve_fit(self.pos_exp_func, x, y)
        self.__set_tpr__()

        # print "Weight with tpr={} is {}".format(self.tpr, self.pos_exp_func(self.tpr, *coeffs))
        tpr_weight = self.pos_exp_func(self.tpr, *coeffs)
        return tpr_weight

    def get_fpr_weight(self, x, y):
        """
        Calculate fpr weight, for a given false positive rate stored as instance variable

        Args:
            x (List of floats): The x points on plot to calculate the equation to fit on
            y (List of floats): The corresponding y values for x-values given

        Returns:
            fpr_weight (Float): The weight assigned to that particular false positive rate.
        """

        coeffs, b = curve_fit(self.exp_func, x, y)
        self.__set_fpr__()

        # print "Weight with fpr={} is {}".format(self.fpr, self.exp_func(self.fpr, *coeffs))
        fpr_weight = self.exp_func(self.fpr, *coeffs)
        return fpr_weight

    def get_fnr_weight(self, x, y):
        """
        Calculate fnr weight, for a given false negative rate stored as instance variable

        Args:
            x (List of floats): The x points on plot to calculate the equation to fit on
            y (List of floats): The corresponding y values for x-values given

        Returns:
            fnr_weight (Float): The weight assigned to that particular false negative rate.
        """

        coeffs, b = curve_fit(self.double_exp_func, x, y)
        self.__set_fnr__()
        # print "Weight with fnr={} is {}".format(self.fnr, self.double_exp_func(self.fnr, *coeffs))
        fnr_weight = self.double_exp_func(self.fnr, *coeffs)
        # plt.figure()
        # plt.plot(x, y, label='Original data')
        # yn = [self.double_exp_func(elem, *coeffs) for elem in x]
        # plt.plot(x, yn, label='Fitted data')
        # plt.legend()
        # plt.show()
        return fnr_weight

    def get_correlation_weight(self, x, y):
        """
        Calculate correlation weight, for a given correlation rate between scores stored as instance variable

        Args:
            x (List of floats): The x points on plot to calculate the equation to fit on
            y (List of floats): The corresponding y values for x-values given

        Returns:
            correlation_weight (Dict of floats): The weight assigned to that particular score correlation.
        """

        self.__get_correlation__()

        coeffs, b = curve_fit(self.exp_func, x, y)

        self.correlation_weights[self.m_score] = dict()

        for model in self.models:
            if model != self.m_score:
                self.correlation_weights[self.m_score][model] = \
                    self.return_correlation_weight(self.correlation[self.m_score]["{}-{}".format(self.m_score, model)],
                                                   coeffs)

        return self.correlation_weights[self.m_score]

    def return_correlation_weight(self, value, coeffs):
        return self.exp_func(value, *coeffs)

    def calculate_rates(self, df=pd.DataFrame()):
        """
        This method is used to calculate various measure rates of model.

        Args:
            df (Pandas DataFrame): The DataFrame object which should have 2 columns, 'actual' and 'prediction' and indexed by timestamps.

        Returns:
            tpr_count (Int): The number of true positives
            fpr_count (Int): The number of False positives
            tnr_count (Int): The number of true negatives
            fnr_count (Int): The number of False negatives
        """

        tpr_count = 0
        fpr_count = 0
        fnr_count = 0
        tnr_count = 0
        # Fetches all the results stores in self.result. Keeps a count of number of 1s & 0s with  actuals.
        for i in range(df.size[0]):
            if df.iloc[i]['actual'] == df.iloc[i]['prediction'] == 1:
                tpr_count += 1
            elif df.iloc[i]['actual'] == 1 and df.iloc[i]['prediction'] == 0:
                fnr_count += 1
            elif df.iloc[i]['actual'] == 0 and df.iloc[i]['prediction'] == 1:
                fpr_count += 1
            elif df.iloc[i]['actual'] == 0 and df.iloc[i]['prediction'] == 0:
                tnr_count += 1

        return tpr_count, fpr_count, tnr_count, fnr_count


if __name__ == '__main__':

    obj = CalculateWeights()
    obj.fit()
