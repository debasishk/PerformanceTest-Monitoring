# Project: Predictive Analytics
# Author: Debasish Kanhar
# UserID: BC03421

# This API is called to standardize scores
# Currently MAD outputs Modified z score
# LOF outputs Density based score
# FFT outputs scores without units. Its difference between points with respect to prev point.
# Modify each score to z-score, and then standardize/normalize in range 0-100.
# Use window size of 'x' for calculating mean and standard deviation.

# Higher the window more smoothed out the scores are after converting.
# If we dont use window, each day, at midnight, window position is resetted.
# Eg: From midnight till next midnight, use all data entered till current time to calculate mean and sigma to calculate z-score.
# Else in case of 'fixed' window, calculate mean and sigma for that 'window' size data for calculating z-score.


import pandas as pd
import os
import numpy as np
from utils.mkdir import mkdir


out_dir = ''
out_file = 'z-score_Outliers_{}.csv'


class ConvertScores(object):
    """
    Class to convert raw scores to z-scores
    """
    def __init__(self):
        self.window = 100
        self.use_window = False

        self.z_score = dict()
        self.f_data = dict()

        pass

    def apply_(self, data, cols=None):
        """
        Apply api for convertScores class

        Args:
            data (Pandas DataFrame):
            cols (List of String):

        Returns:
            z_score (Dictionary of Pandas DataFrame):
        """

        if cols:
            self.cols_to_convert = cols
        else:
            self.cols_to_convert = data.columns.tolist()
            self.cols_to_convert = [elem for elem in self.cols_to_convert if elem != 'timestamp']

        self.score_df = data

        self.__driver__()

        return self.z_score

    def __driver__(self):
        """
        Driver method for ConvertScores class to call all sub methods.
        """

        for col in self.cols_to_convert:

            data = self.score_df[[col]]

            new_scores = self.modify_data(data, col)

            self.z_score[col] = new_scores

        pass

    def modify_data(self, data, col):
        """
        Convert Raw scores to Z-scores.

        Args:
            data (Pandas DataFrame): DataFrame storing raw scores
            col (String): The column on which conversion is being done

        Returns:
            z_score (Pandas DataFrame): New DataFrame storing z-scores instead of raw scores.
        """

        # Now calculate z-score based on rolling windows for columns calculated above.

        z_score = pd.DataFrame()

        score = data
        z_scores = []

        ctr = 0

        if type(score.index.tolist()[0]) is pd.tslib.Timestamp:
            # When score dataframe object is indexed by timestamp
            index = score.index.tolist()

            score.reset_index(inplace=True)

            try:
                score.drop('timestamp', axis=1, inplace=True)
            except ValueError as e:
                score.drop('index', axis=1, inplace=True)

        for idx in score.index.tolist():
            row = score.ix[idx]

            n = min(idx, self.window)

            if self.use_window:
                subdata = score.iloc[ctr-n:ctr+1]
            else:
                subdata = score.iloc[:ctr+1]

            x = row[0]
            mu = subdata.mean()
            sigma = subdata.std()

            new_score = (x-mu)/sigma

            z_scores = np.append(z_scores, new_score)
            # z_scores.append(new_score)

            ctr += 1

            z_scores = np.array(z_scores)

        z_score = pd.DataFrame(data=z_scores, index=index)

        return z_score

    def get_servers(self, data):
        """
        This method return the server names for which scores are present in dataset.

        Args:
            data (Pandas DataFrame): The DataFrame storing all the scores from where server names to be found out

        Returns:
            servers (List of String): The server names inferred from DataFrame columns stored in list.
        """
        cols = data.columns.tolist()
        servers = []
        data_servers = []

        for col in cols:
            if '_actual' in col:
                servers.append(col.split('_actual')[0])

        for col in cols:
            if '_actual' in col or '_diff' in col:
                data_servers.append(col)

        self.data_servers = data_servers

        return servers

    def _full_data_(self):
        """
        Returns the full dataset.

        Returns:
            self.f_data (Pandas DataFrame): The full dataset which is to be returned.
        """
        return self.f_data

    def full_data(self, data):
        da = data[self.data_servers]
        return da

    def __get_data__(self, file):
        """
        Method to fetch data from csv files.

        Args:
            file (String): File name from which data to be fetched.

        Returns:
            data (Pandas DataFrame): The csv file read into DataFrame
        """
        data = pd.read_csv(file, index_col=['timestamp'])
        os.remove(file)
        return data

    def _z_scores_(self):
        """
        Method to return z_scores calculated in previous steps.

        Returns:
            self.z_score (Dictonary of Pandas DataFrame): The dictionary storing all the z-scores with key as metric name and value as z-scores DataFrame
        """
        return self.z_score


class ScaleScores(object):
    """
    Class to scale scores in specified range
    """
    def __init__(self, scores):
        """
        Constructor to initialize ScaleScores class.

        Args:
            scores (Dictionary of Pandas DataFrame): The dictionary storing all the z-scores which needs to be scaled with key as metric name and value as z-scores DataFrame
        """
        self.scores = scores
        # print scores
        self.scaled_scores = dict()

        self.__driver__()
        pass

    def __driver__(self):
        """
        Driver method for ScaleScores class.

        Fetches z-score DataFrame for a specific metric.
        Scales the z-scores based on new ranges required.
        Saves it to scaled_scores dictionary in same format as z_score was saved.

        """

        for metric in self.scores.keys():
            df = self.scores[metric]
            scaled = pd.DataFrame()
            ts = df.index.tolist()

            for col in df:
                tmpDF = df[[col]]

                min = tmpDF.min()
                max = tmpDF.max()

                tmpDF[col] = tmpDF[col].apply(self.scale_scores, args=(min, max))
                # print tmpDF[col]
                scaled[col] = tmpDF[col].values.tolist()

            scaled['timestamp'] = ts
            scaled.set_index(keys='timestamp', inplace=True)

            self.scaled_scores[metric] = scaled

        pass

    def scale_scores(self, x, oldMin, oldMax):
        """
        Scale scores in new range specified. Here it is in range 0-100.

        Args:
            x (Float): The score which needs to be scaled. Can be raw score or z-score or anything.
            oldMin (Float): Old Range's minimum value
            oldMax (Float): Old Range's maximum value

        Returns:
            newValue (Float): The newly scaled value
        """
        oldRange = oldMax - oldMin
        newRange = 100 - 0
        newValue = (((x-oldMin) * newRange)/oldRange) + 0
        return newValue

    def _scaled_scores_(self):
        """
        Method to return scaled scores.

        Returns:
            self.scaled_scores (Dictionary of Pandas DataFrame): Scaled scores saved in dictionary where key is metric name, and value against it is scaled scores in Pandas DataFrame
        """
        return self.scaled_scores


def main():
    """
    Method to initialize conversion followed by scaling of scores from raw scores in any range to z-score in range 0-100.

    Examples:
        For using this class, follow the exact same code mentioned in this method, else follow the following example::

        convertscores = ConvertScores()
        z_scores = convertscores._z_scores_()

        scalescores = ScaleScores(z_scores)
        z_scores = scalescores._scaled_scores_()

    """
    global out_file

    convertscores = ConvertScores()
    z_scores = convertscores._z_scores_()
    full_data = convertscores._full_data_()

    scalescores = ScaleScores(z_scores)
    z_scores = scalescores._scaled_scores_()

    for metric in z_scores.keys():
        dir = out_dir + metric + '\\'
        mkdir(dir)
        file = dir + out_file

        df1 = z_scores[metric]
        df2 = full_data[metric]
        df = pd.concat([df1, df2], axis=1)

        df_final = df.reindex_axis(sorted(df.columns), axis=1)

        df_final.to_csv(file)
        # print "Saved scaled scored to {} for {}".format(file, metric)

if __name__ == '__main__':
    main()
