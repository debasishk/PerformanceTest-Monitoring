"""
Distance-based outlier detection model with optional intensional knowledge extraction.
"""

from __future__ import division
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import itertools
import scipy.spatial.distance as dist


class DB:
    """Object that applies the distance-based outliers model with optional
    intensional knowledge extraction.
    
    Keyword arguments:
        p (float): Takes values in the range (0.00, 1.0), the critical percentage
            describing how "full" a data point's neighborhood must be in order
            to be flagged as NOT an outlier
        distance_method (str): One of {'random sampling', 'quantile', 'median',
            'mean'} -- the method used to calculate the critical distance d,
            which describes the length of the radius that defines a data point's
            neighorhood in a given attribute space.
        ik (bool): intensional knowledge extraction flag -- denotes whether
            or not to build a hierarchy out of the distance-based outliers
            model output and apply the intensional knowledge model on it.
        output_csv (bool): Flag denoting whether or not to generate a CSV file
            of the model output.
        plot (bool): Flag denoting whether or not to plot the model output
    
    Attributes:
        p (float): Takes values in the range (0.00, 1.0), the critical percentage
            describing how "full" a data point's neighborhood must be in order
            to be flagged as NOT an outlier
        distance_method (str): One of {'random_sampling', 'quantile', 'median',
            'mean'} -- the method used to calculate the critical distance d,
            which describes the length of the radius that defines a data point's
            neighorhood in a given attribute space.
        ik (bool): intensional knowledge extraction flag -- denotes whether
            or not to build a hierarchy out of the distance-based outliers
            model output and apply the intensional knowledge model on it.
        output_csv (bool): Flag denoting whether or not to generate a CSV file
            of the model output.
        plot (bool): Flag denoting whether or not to plot the model output
    """
    
    def __init__(self, p=0.05, distance_method='random_sampling', ik=False, output_csv=False,
                 plot=False, params=None):
        """Initializes model parameters as instance variables."""
       
        #Model parameter(s)
        self.p = p
        self.distance_method = distance_method
        self.ik = ik
        
        #I/O variables
        self.output_csv = output_csv
        self.plot = plot
        
        if params != None:
            self.p = params['p']
            self.distance_method = params['distance_method']
            self.ik = params['ik']
    
    def __str__(self):
        
        return 'DB_IK'
    
    def __repr__(self):
        
        return 'DB_IK(p={}, distance_method={}, ik={})'.format(self.p, self.distance_method, self.ik)
        
    def apply_(self, data, cols=None):
        """Apply the DB_IK model to all specified metrics.
        
        Applies the DB_IK model to all columns specified in the self.columns instance variable.
        The model follows these steps:
        1. Create attribute-space hierarchy from input dataset and cols arguments
        2. Calculate the critical distance (d) for each attribute space
        3. For each data point in each attribute space, calculate how "full" their neighborhood is.
        4. Classify outliers as strong, weak, or trivial and outlying attribute spaces as strong or weak.
        
        Arguments:
            data (DataFrame): the input dataset
        
        Keyword Arguments:
            cols (List[str]): the set of attributes (columns) that comprise the
                topmost node in the attribute-space hierarchy. If None then all
                columns in data are used.
        
        Returns:
            outlier_score_df (DataFrame): dataframe of outlier scores
        """
        
        if cols == None:
            cols = data.columns.values
        
        #create set of all possible column combinations
        all_column_combination_set = self.create_all_column_combination_set(cols=cols)
        
        #initialize output dataframe
        output_df = pd.DataFrame(index=data.index)
        
        #loop over all different column/metric combinations
        for column_set in all_column_combination_set:
            
            #print '\tAttributes:',', '.join(column_set)
            
            #create dataset based on this iteration's metrics
            df = data[ list(column_set) ]
            
            #find critical distance (d parameter)
            self.find_d(df, self.distance_method)

            #check data for integrity            
            if self.d == 0.0:
                #print '\t\tunable to run model'
                continue
            
            #find outliers
            outlier_info_list = self.find_outliers(df, plot=self.plot)
            
            #merge outlier information with dataset
            output_df = self.merge_output(data, output_df, outlier_info_list,column_set)
            
        #create output datasets in various formats (MySQL table or CSV)
       
        if self.output_csv:
            output_df.to_csv('db_info.csv')
        
        #create attribute hierarchy object
        if self.ik:
            self.hierarchy = Intensional_Knowledge(data, cols)
            self.hierarchy.driver(output_textual_tree=False)
        
        #generate outlier score
        outlier_score_df = self.generate_outlier_score(data)
        return outlier_score_df
    
    def generate_outlier_score(self, data):
        """Generate outlier score for each timestamp in data argument.

        Outlier score for this method is defined as following:
        scores = (1 - min(neighborhood % filled for each attribute space))*10
        
        Arguments:
            data (DataFrame): the initial dataset
            cols (List[str]): the set of attributes (columns) that comprise the
                topmost node in the attribute-space hierarchy
        
        Returns:
            scores_df (DataFrame): dataframe containing outlier scores
        """  
        
        #create and populate a list of all pct columns
        pct_cols = []
        for col in list(data.columns.values):            
            if 'pct' in col:
                pct_cols.append(col)
            
        #create and populate list of timestamps
        ts_list = data.index.tolist()
        
        #instantiate list to hold outlier scores
        scores = []        
        
        #calculate outlier scores
        for ts in ts_list:
            
            pcts = data.loc[ts,pct_cols].values.tolist()
            score = (1 - min(pcts))*10
            scores.append(score)
            
        #create dataframe out of scores
        scores_df = pd.DataFrame(scores, index=ts_list, columns=['DB_outlier_score'])
        
        return scores_df    

    def find_d(self, df, method='random_sampling'):
        """Calculate the radius of the (hyper) sphere that defines the neighborhood 
        of the input dataframe's attribute space.
        
        This method finds the critical distance d parameter. This value is the
        distance that defines a data point's neighborhood for the attribute-space
        defined by the  columns of the df argument. All datapoints within
        the distance d are defined as inside a data point's neighborhood.
        d is calculated via 3 different methods:
        1. quantile - calculates the range of the 75th and 25th quantile for each
        column in df. A vector of range values is then used to calculate the euclidean
        distance from this vector to the origin in k-space (k being the number of 
        columns in df)
        2. median - calculates the median of each column in df. A vector of median
        values is then used to calculate the euclidean distance from this vector to the origin in k-space.
        3. mean - same as 2 but with the mean
        4. random sampling - extract a random sample of 25% of input data. The median distance
        between all points in the random sample is used as the critical distance
        
        Arguments:
            df (DataFrame): the dataset considered/used when finding outliers
            
        Keyword arguments:
            distance_method (str): One of {'random_sampling', 'quantile', 'median',
                'mean'} -- the method used to calculate the critical distance d,
                which describes the length of the radius that defines a data point's
                neighorhood in a given attribute space.    
        
        Attributes:
            d (float): the radius of the (hyper) sphere that defines the neighborhood
                of the input dataframe's attribute space.
        """
        
        if method == 'quantile':
            
            #calculate the 75th and 25th quantiles of each column in df       
            quantile_25 = df.quantile(0.25).values.tolist()
            quantile_75 = df.quantile(0.75).values.tolist()
        
            #compute quantile range
            distance_vector = [ quantile_75[i] - quantile_25[i] for i in range(len(quantile_25)) ]
        
            #construct vector of 0's
            zero_vector = [0]*len(distance_vector)
          
            #calculate distance between zero vector and distance vector
            self.d = dist.euclidean(zero_vector,distance_vector)
        
        elif method == 'median':
            
            #calculate median for each column in df
            medians_dict = df.median().to_dict()
            
            #create vector from medians
            median_vector = list(medians_dict.values())
            
            #construct vector of 0's            
            zero_vector = [0] * len(median_vector)
            
            #calculate distance between zero vector and median vector
            self.d = dist.euclidean(zero_vector,median_vector)
        
        elif method == 'mean':
            
            #calculate mean for each column in df
            means_dict = df.mean().to_dict()
            
            #create vector from means            
            mean_vector = list(means_dict.values())
            
            #construct vectors of 0's
            zero_vector = [0] * len(mean_vector)
            
            #calculate distance between zero vector and mean vector
            self.d = dist.euclidean(zero_vector, mean_vector)
            
        elif method == 'random_sampling':
            
            #randomly pick 25% of data points from initial dataset
            random_sample = df.sample( frac=0.25 )
            
            #compute distances between all points in random sample
            all_distances = []
            for row in random_sample.itertuples():
                
                #save necessary index and values
                reference_timestamp = row[0]
                reference_vals = row[1:]
                
                #create new dataset with all all rows but reference row
                all_values_but = random_sample[ random_sample.index != reference_timestamp ]
                
                for row2 in all_values_but.itertuples():
                    
                    #compute distance
                    temp_vals = row2[1:]
                    temp_dist = dist.euclidean(reference_vals,temp_vals)
                    all_distances.append(temp_dist)
            
            #calculate median of distances
            all_distances.sort()
            median_distance = np.median(all_distances)
            self.d = median_distance
        
        #print '\t\td:',self.d
            
            
    def merge_output(self, data, output_df, outlier_info_list, column_set):
        """
        Merge outlier flag list with the original dataset.
        
        Arguments:
            output_df (DataFrame): dataframe to merge the outlier information with
            outlier_info_list (List[tup]): a list containing timestamps and outlier flags 
                of the form [(timestamp, outlier_flag, pct_close), (timestamp, 
                outlier_flag, pct_close), ...]
            column_set (1D list): a list containing the names of the columns in
                the current dataset
      
        Returns:
            output_df (DataFrame): original dataframe argument augmented with model
                output.
        """
        
        #create column names and lists of values
        outlier_column_name = '_'.join(column_set+('outlier',))
        pct_column_name = outlier_column_name.replace('outlier','pct')
        ts_list = [i[0] for i in outlier_info_list]
        flag_list = [i[1] for i in outlier_info_list]
        pct_list = [i[2] for i in outlier_info_list]
    
        #add new column to original dataset
        data.loc[:, outlier_column_name] = flag_list
        data.loc[:, pct_column_name] = pct_list        
        
        #add new column to output dataset
        output_df[outlier_column_name] = flag_list
        output_df[pct_column_name] = pct_list
    
        return output_df
    
    def find_outliers(self, df, plot=False):
        """
        Apply the DB(p,d) model.        
        
        This method implements the DB(p,d) method for finding outliers.
        DB(p,d) first calculates the number of data points within each data point's
        neighborhood. A data point's neighborhood is defined by d. See the
        method find_d for details. If the proportion of data points within a
        neighborhood to the total number of data points in the data set exceeds
        the critical percentage p, then the data point in question is considered
        to NOT be an outlier.
        
        Arguments:
            df (DataFrame): the dataset in which to find outliers
        
        Keyword Arguments:
            plot (bool): flag denoting whether or not to plot model output
        
        Returns:
            outlier_flag_list (List[tup]): a list containing timestamps and outlier flags
                of the form [(timestamp, flag, pct), (timestamp, flag, pct), 
                (timestamp, flag, pct), ...]
        """

        outlier_flag_list = []        
        
        #loop over all data points in df
        for row in df.iterrows():
       
            #save timestamp and data values of the current iteration's data point
            reference_timestamp = row[0]
            reference_vals = row[1]
            
            #select all data points EXCEPT the one in the current iteration
            all_values_but = df[ df.index != reference_timestamp ]
            
            #calculate the number of existing data points
            total_other_data_points = len(all_values_but)
            num_close_data_points = 0
            
            #initialize outlier_flag to be 1
            outlier_flag = 1
            
            #loop over all values except the reference value
            for data_point in all_values_but.iterrows():
                
                #save timestamp and data values of current comparison point
                temp_timestamp = data_point[0]
                temp_vals = data_point[1]
                
                #calculate the distance between the reference data point and the current data point
                distance = dist.euclidean(reference_vals,temp_vals) 
                
                #check if the distance between the reference data point and the current data point
                #is less than the critical outlier distance (defined by self.d)
                if distance < self.d:
                    
                    #increment the number of data points considered to be close to the reference data point
                    num_close_data_points += 1
    
                #calculate the percent of all data points that lie within the neighborhood of the reference
                #data point (defined by self.d)
                pct_close = num_close_data_points/total_other_data_points
                
                #check if pct_close is greater than the critical percentage used to flag an outlier
                if pct_close > self.p:
                    
                    outlier_flag = 0
                    #break
            
            #save outlier flag
            outlier_flag_list.append((reference_timestamp,outlier_flag,pct_close))
            
        
        if plot:
            
            columns = list(df.columns.values)
            
            if len(columns) == 3:
                plot_type = '3D'
            elif len(columns) == 2:
                plot_type = '2D'
            elif len(columns) == 1:
                plot_type = '1D'
            
            
            if plot_type == '3D':
                
                fig = plt.figure()
                ax = fig.add_subplot(111, projection='3d')
                
                x_raw = self.data[ columns[0] ].values.tolist()
                y_raw = self.data[ columns[1] ].values.tolist()
                z_raw = self.data[ columns[2] ].values.tolist()
                
                color = ['b' if i[1] == 0 else 'r' for i in outlier_flag_list]
                
                ax.scatter(x_raw,y_raw,zs = z_raw,c = color, s = 40)
                ax.set_xlabel(columns[0])
                ax.set_ylabel(columns[1])
                ax.set_zlabel(columns[2])
            
            if plot_type == '2D':
                
                fig = plt.figure()
                
                x = self.data[ columns[0] ].values.tolist()
                y = self.data[ columns[1] ].values.tolist()
                
                color = ['b' if i[1] == 0 else 'r' for i in outlier_flag_list]
                
                plt.scatter(x,y,c = color, s = 40)
                plt.xlabel(columns[0])        
                plt.ylabel(columns[1])
                
                
            if plot_type == '1D':
                
                fig = plt.figure()
                
                y = self.data[ columns[0] ].values.tolist()
                x = [i for i in range(len(y))]
                
                outliers = [y[i] if outlier_flag_list[i][1] == 1 else None for i in range(len(y))]
                
                plt.plot(y,'b-')
                plt.xlabel('time')
                plt.ylabel(columns[0])
                plt.plot(x,outliers,'ro')
                
        return outlier_flag_list
        
        
    def create_all_column_combination_set(self, cols=None):
        """Create all possible column combinations based on original dataset.
        
        Keyword Arguments:
            cols (List[str]): the set of attributes (columns) that comprise the
                top-most node in the attribute-space hierarchy.
        
        Returns:        
            all_column_combinations_set (List[List[str]]): set of all possible
                combinations of columns specified by cols.
        """
        
        # instantiate output variable
        all_column_combination_set = [tuple(cols)]
        
        #populate list with all column combinations
        i = len(cols)-1
        while i > 0:
            subset = list(itertools.combinations(cols,i))
            all_column_combination_set += subset
            i -= 1
        
        return all_column_combination_set
        
    
    def test(self, test_df, columns=None):
        """Test intensional knowledge extraction of distance-based outliers model (DB_IK).
        
        Method used for unit tests -- calculates outlier score for each data point in
        the test dataset, for each attribute space (including subspaces) supplied in the
        columns argument.
        
        Arguments:
            test_df (DataFrame): the input test dataset
        
        Keyword arguments:
            columns (List[str]) - list of the columns to to use when calculating
                the DB_IK scores
        
        Returns:
            test_output (DataFrame) - one dimensional dataframe with scores for
                reach data point (row)
        """
        
        #extract columns if necessary
        if columns != None:
            test_df = test_df[ columns ]
        if columns == None:
            columns = test_df.columns.values
        
        #apply the model
        test_output = self.apply_(test_df, columns)
        
        #return the output        
        return test_output
    


class Attribute_Node:
    """Node representation of an attribute subspace.
    
    Attributes:
        attributes (tup): tuple of raw attribute names (columns) that
            define this node's attribute space.
        outliers (DataFrame): dataframe containing outlier flags for each
            timestamp (datapoint).
        name (str): name describing the attribute (sub)space that this node
            represents.
        parents (List[Attribute_Node]): list of nodes which qualify as parents
            of this node.
        children (List[Attribute_Node]): list of nodes which qualify as children
            (true sub-attribute spaces) of this node.
        peers (List[Attribute_Node]): list of nodes which contain the same number
            of attributes as this node comprising the attribute space they
            represent.
        classification (str): representation of the outlying "strength" of
            this node.
        num_outliers (int): the number of outliers of this node's attribute space
    """    
    
    def __init__(self, attributes, outliers):
        """Initializes relevant instance variables."""
        
        self.attributes = attributes
        self.outliers = outliers
        
        self.name = '_'.join(self.attributes)
        
        self.parents = []
        self.children = []
        self.peers = []
        
        self.classification = None
        
        self.get_num_outliers()

    def __str__(self):
        
        return self.name
        
    def __repr__(self):
        
        return self.name

    def get_num_outliers(self):
        """Calculate the number of outliers of the given node (i.e. attribute space)"""
        
        outlier_set_name = self.name+'_outlier'
        self.num_outliers = len(self.outliers[ self.outliers[ outlier_set_name ] == 1 ])


    def add_child(self, attributes, outliers):
        """Create the child node of the input attributes list and outlier dataframe.
        
        First, this method checks if a node with the inputted attributes already exists.
        If it does, then no new child node is created and the existing child node is used.
        This method updates the parents, children, and peers lists of all nodes
        affected by the addition of this new child (or addition of relationship i.e. edge).
        
        Arguments:
            attributes (List[str]): list containing all attributes of the given
                sub-attribute space.
        
            outliers (DataFrame): all timestamps and associated outlier flags of
                the given sub-attribute space.
        
        Returns:
            child_node (Attribute_Node): new or existing child node
        """
        
        #check if an object already exists in the hierarchy with this set of attributes
        peer_children = self.get_peer_children()
        child_already_exists = self.check_for_existing_child(attributes,peer_children)

        #if the child doesn't already exist then create an Attribute_Node with its attributes and outliers
        if child_already_exists == False:
        
            child_node = Attribute_Node(attributes,outliers)
            
        
        #if the child does already exist, point to it
        else:
            child_node = child_already_exists
   
        #add the current node to the child node's list of parents
        child_node.parents.append(self)
    
        #add the newly created/found child node to the list of children of the current node     
        self.children.append(child_node)
     
        #update the list of peers for each child node of the current node's peers
        self.update_child_peers(child_node,peer_children)
        
        #update peers of the current node
        self.update_peers()
        
        return child_node
        
    
    def update_peers(self):
        """Update peer lists of all peers to include the current node and vice versa."""
        
        #loop over all parents
        for parent in self.parents:
            
            #loop over all children of my parents
            for parent_child in parent.children:
                
                #add self if self isn't already a peer
                if self not in parent_child.peers and parent_child != self:

                    parent_child.peers.append(self)
                    
                #add child to peers if it isn't already in the peer list
                if parent_child not in self.peers and parent_child != self:
                    
                    self.peers.append(parent_child)
        
    def update_child_peers(self, child_node, peer_children):
        """
        Update the list of peers for each child in the current node's peer list
        to include the child node argument passed in.
        
        Arguments:
            child_node (Attribute_Node): an object representing the newly created/
                just found child node
            peer_children (List[Attribute_Node]): list of Attribute_Node objects 
                representing the current node object's peer's children
        """        

        #loop over all my children
        for child in self.children:
            
            #loop over all children except the one already being considered
            temp_children = [i for i in self.children if i != child]
            for child2 in temp_children:
                
                #check for equivalency (redundant) and if compared child isn't in first child's list of peers
                if child != child2 and child2 not in child.peers:
                    
                    child.peers.append(child2)       
        
        #loop over each child node object of the peers
        for child in peer_children:
            
            #update peer list if current node isn't already a peer
            if child_node not in child.peers:
                child.peers.append(child_node)
                
    
    
    def check_for_existing_child(self, attributes, peer_children):
        """
        Checks if the attributes passed in are already represented as a node 
        in the hierarchy. If they are, then return the object, if not, return False

        Arguments:
            attributes (List[str]): list representing the attributes that describe a node
                in the attribute space hierarchy.
            peer_children (List[Attribute_Node]): list of Attribute_Node objects
                which are the children of the current node's peers.
        """
        
        #loop over each child node object
        for child in peer_children:
            
            #get attributes
            child_attributes = child.attributes
            
            #check if attributes are the same
            if child_attributes == attributes:
                return child
        
        #return False if all child objects are considered but no match is found
        return False
        
        
    def get_peer_children(self):
        """Creates a list of all the current node object's peer's children."""
    
        peer_children = []        
        for peer in self.peers:
            for child in peer.children:
                peer_children.append(child)
        
        return peer_children
        
    def print_final_outlier_classification_traverse(self):
        """Traverse tree, prints out all outlier final classifications."""
        
        print ('\t'*len(self.attributes),str(self))
        print ('\t'*len(self.attributes),'STRONG')
        print (self.outliers[ self.outliers['type'] == 'strong' ])
        print ('\t'*len(self.attributes),'WEAK')
        print (self.outliers[ self.outliers['type'] == 'weak' ])
        print ('\t'*len(self.attributes),'TRIVIAL')
        print (self.outliers[ self.outliers['nontrivial'] == 0.0 ])
        
        for child in self.children:
            child.print_final_outlier_classification_traverse()
        
        
    def print_strength_classification_traverse(self):
        """Traverses tree, prints out strength classification"""
        
        print ('\t'*len(self.attributes),str(self))
        print (self.num_outliers)
        print (self.classification)
        
        for child in self.children:
            child.print_strength_classification_traverse()
        
    def print_outliers_traverse(self):
        """Traverses tree, prints out outlier datapoints."""
        
        print ('\t'*len(self.attributes),str(self))
        outlier_set = self.outliers[ self.outliers[ self.name+'_outlier' ] == 1]
        print(outlier_set)
        
        for child in self.children:
            child.print_outliers_traverse()
            
    def print_nontrivial_traverse(self):
        """Traverses tree, prints out nontrivial flag values"""
        
        print ('\t'*len(self.attributes),str(self))
        nontrivial_set = self.outliers[ self.outliers[ 'nontrivial'] == 1]
        trivial_set = self.outliers[ self.outliers[ 'nontrivial'] == 0]
        print (nontrivial_set)
        print (trivial_set)
        
        for child in self.children:
            child.print_nontrivial_traverse()
    
    def print_traverse(self):
        """Traverses tree, prints out node names"""
        
        print ('\t'*len(self.attributes)+str(self))
        print ('\t'*len(self.attributes),'Parents:',self.parents)
        print ('\t'*len(self.attributes),'Peers:',self.peers)
        print ('\t'*len(self.attributes),'Children:',self.children)
        
        for child in self.children:
            child.print_outliers_traverse()
                

class Intensional_Knowledge:
    """Object which augments the DB model, extracts intensional knowledge from
    distance based outliers.
    
    Arguments:
        data (DataFrame): the initial dataset.
        cols (List[str]): the set of columns in data to use when running the
            model.
    
    Attributes:
        data (DataFrame): the initial dataset.
        root_attrs (List[str]): the set of columns in data to use when running the
            model.
    """
    
    def __init__(self, data, cols):
        """Initialize necessary instance variables."""
            
        #instantiate object variables
        self.data = data
        self.root_attrs = cols
        
    def driver(self, output_textual_tree=False):
        """Run the model.
        
        Keyword Arguments:
            output_textual_tree (bool): flag denoting whether or not to output
                the attribute space hierarchy (i.e. tree).
        """
        
        #Method calls
        self.build_hierarchy(self.root_attrs)
        self.classify_outliers_trivial_nontrivial()
        self.classify_attribute_space()
        self.classify_outliers_strong_weak()
        
        if output_textual_tree:
            self.output_textual_tree()    
        
    def build_hierarchy(self, cols):
        """Build data structure representing hierarchy of attributes spaces.

        Given dataset with transactions, CPU, and Memory usage, the following tree structure is generated.
        -Transactions_CPU_Memory
             --CPU_Memory
                ---CPU
                ---Memory
            --Transactions_CPU
                ---CPU
                ---Transactions
            --Transactions_Memory
                ---Transactions
                ---Memory
            
        Arguments:
            cols (List[str]): list of the attributes which comprise the root node
        """
        
        #create root node of attribute hierarchy
        self.build_root_node(cols)
        
        #initial call to recursive method which builds the hierearchy out of the root node
        self.add_node(self.root)
        
    
    def output_textual_tree(self):
        """
        Instantiate the process to traverse the entire attribute hierarchy while
        printing outlier information and attribute space classification for each node.
        """
        
        #instantiate needed variables (iteration/call count and pointer to the root)
        i = 0
        current_node = self.root
        
        #first recursive call to traversal method
        self.traverse_tree_generate_outlier_report(current_node,i)
    
    def traverse_tree_generate_outlier_report(self, node, i):
        """
        Traverse the attribute-space tree and print out outlier information and
        node/attribute space classification for the node argument passed in.
        This method is recursive.

        Arguments:
            node (Attribute_Node): object representing the current node of the 
                hierarchy the traversal method is on.
            i (int): integer representing the "depth level" of each node. This 
                is just used for formatting the output.
        """
        
        #Prints out relevant information
        print ('\t'*i,node.name.upper())
        print ('\t'*i,'Attribute (sub)space classification:','strong' if node.classification == 1 else 'weak')
        print ('\t'*i,'Strong Outliers:',[row[0] for row in list(node.outliers[ node.outliers['type'] == 'strong' ].loc[:,'type'].iteritems())])
        print ('\t'*i,'Weak Outliers:',[row[0] for row in list(node.outliers[ node.outliers['type'] == 'weak' ].loc[:,'type'].iteritems())])
        print ('\t'*i,'Trivial Outliers:',[row[0] for row in list(node.outliers[ node.outliers['type'] == 'trivial' ].loc[:,'type'].iteritems())])
        
        #increment i
        i += 1
        
        #recursively call this method for all child nodes in the hierarchy
        for child in node.children:
            self.traverse_tree_generate_outlier_report(child,i)

    def classify_outliers_strong_weak(self):
        """
        Traverse hierarchy, touching each attribute space and for each outlier 
        in each attribute space, classify the outlier as strong or weak.
        Definition: Suppose that P is a non-trivial outlier in the attribute space A_p. Then if P is not
        a strongest outlier, P is called a weak outlier
        """
        
        self.outer_outlier_strength_traversal(self.root)
        
    def outer_outlier_strength_traversal(self,node):
        """
        Traverse the entire hierarchy (tree) classifying outliers at each
        node as strong or weak. This method is recursive.
        
        Arguments:
            node (Attribute_Node): node (i.e. attribute space) in the attribute
                hierarchy.
        """
        
        #initialize strong column in outlier dataset for each node
        try:        
            #initialize strong column in outliers dataset
            node.outliers.insert(2,'type',[np.nan] * len(node.outliers))
        except:
            #print 'strong column already created for node {}'.format(list(node.attributes))
             pass   
         
        #construct outlier set name and extract all outlier timestamps        
        outlier_set_name = '_'.join(node.attributes)+'_outlier'        
        node_outliers = node.outliers[ node.outliers[outlier_set_name] == 1 ].loc[:,'nontrivial']
        
        for outlier in node_outliers.iteritems():
            
            ts = outlier[0]
            nontrivial_flag = outlier[1]            
            
            if nontrivial_flag == 1.0 and node.classification == 1:
                node.outliers.loc[ts,'type'] = 'strong'
            elif nontrivial_flag == 1.0 and node.classification == 0:
                node.outliers.loc[ts,'type'] = 'weak'
            elif nontrivial_flag == 0.0:
                node.outliers.loc[ts,'type'] = 'trivial'
            
        #terminating condition -- if the current node has no children then it must be a 
        #leaf node so stop traversing the tree        
        if node.children == []:
            return        
        
        # recursive calls
        for child in node.children:
            self.outer_outlier_strength_traversal(child)
                
    def classify_attribute_space(self):
        """
        Execute the first recursive method call of outer_attribute_strength_traversal.
        
        Traverse the hierarchy to classify each sub-attribute space as a strongest
        outlying space or weak outlying space.
        Definition: Let A_p be an attribute space containing one or more outliers. A_p is called a STRONGEST OUTLYING SPACE
        if no outlier exists in any proper subspace B of A_p. Further, any P that is an outlier in A_p is called
        a STRONGEST outlier.     
        """
        
        #initial recursive method call
        self.outer_attribute_strength_traversal(self.root)        
        
    def outer_attribute_strength_traversal(self, node):
        """
        Traverse the entire hierarchy and classify each attribute/node as strong
        or weak.
        
        Arguments:
            node (Attribute_Node): the node to start traversing from
        """
        
        #traverse hierarchy/tree, calculate classification
        node.classification = self.inner_attribute_strength_traversal(node,None)        
        
        #terminating condition
        if node.children == []:
            return 

        #recursive call which traverses the rest of the hierarchy/tree        
        for child in node.children:
            self.outer_attribute_strength_traversal(child)
            
    def inner_attribute_strength_traversal(self, node, current_strength_flag):
        """
        Traverse the hierarchy starting from the node argument and calculate the
        strength classification of each attribute space.
        
        Arguments:
            node (Attribute_Node): object representing the node in the hierarchy
                to traverse from.
            current_strength_flag (int): flag used to describe the current strength
                classification of the initial node. One of {0, 1, None} where 
                0 = 'weak', 1 = 'strong', and None = initial call.
        
        Returns:
            current_strength_flag (int): flag used to describe the current strength
                classification of the initial node. One of {0, 1} where 
                0 = 'weak' and 1 = 'strong'
        """
        
        #determine the current strength classification of the initial node argument
        if current_strength_flag == None:
            current_strength_flag = 1 if node.num_outliers > 0 else 0
        else:
            current_strength_flag = 1 if node.num_outliers == 0 and current_strength_flag == 1 else 0
        
        #terminating condition 1: if the current node doesn't have any outliers (i.e. current strength flag = 0)
        #then the original input node (which is at a higher tier) cannot be a strongest outlying space, so terminate
        if current_strength_flag == 0:
            return current_strength_flag

        #terminating condition 2: if we have reached max depth, stop traversing
        if node.children == []:
            return current_strength_flag
        
        #recursive call
        for child in node.children:
            return self.inner_attribute_strength_traversal(child, current_strength_flag)            
            
        
    def add_node(self, current_node):
        """
        Create child nodes of the current_node argument.
        
        Create child nodes of the current_node argument. Child nodes are all 
        subset combinations of attributes of the node passed in. 
        For example, if current_node has 3 attributes: [transactions, CPU, Memory], then
        this method will create all subset combinations of length 2 out of the original
        set of attributes. So we would have [transactions, CPU], [transactions, Memory],
        and [CPU, Memory] created as child nodes of the current_node.
        
        Arguments:
            current_node (Attribute_Node): node object representing the node to
                find/create the children of.
        """
        
        #calculate the next recursive call's attribute cardinality
        i = len(current_node.attributes) - 1
        
        #terminating condition: if there are no attributes left to create nodes out of,
        #then terminate!
        if i == 0:
            return
        
        #create all possible subsets of length i
        subsets = list(itertools.combinations(current_node.attributes, i))

        #create nodes out of each possible subset
        for subset in subsets:
            
            #get outliers from input dataset
            outliers = self.get_outliers(subset)
            
            #create child node
            new_child_node = current_node.add_child(subset, outliers)
            
            #add child node            
            self.add_node(new_child_node)
    
    def build_root_node(self, cols):
        """Create an Attribute_Node object which represents the root node of the attribute hierarchy.
        
        Arguments:
            cols (List[str]): list containing the attributes which represent the
                root node of the attribute hierarchy.
        
        Attributes:
            root (Attribute_Node): object representing the root node of the
                attribute hierarchy.
        """
        
        #get all outliers for the set of attributes for the root
        outliers = self.get_outliers(cols)
        
        #create Attribute_Node object which represents the root node of the attribute hierarchy
        self.root = Attribute_Node(cols, outliers)
        
            
    def get_outliers(self, attributes):
        """Extract outlier column based on attributes argument from initial dataset.
        
        Arguments:
            attributes (List[str]) - list of raw attributes e.g. ['CPU', 'Memory']
        
        Returns:
            outlier_set (DataFrame): single-column dataframe containing the outlier flags from
                the distance-based outlier detection dataset -- specific to the attributes passed in
                e.g. CPU_Memory_outlier as the one column
        """
        
        #construct column name        
        outlier_set_name = '_'.join(attributes)+'_outlier'
        
        #extract column        
        outlier_set = self.data.loc[:,outlier_set_name].to_frame()
        return outlier_set
        
    def outer_tree_traversal(self, node):
        """Traverse the attribute hierarchy (i.e. tree) from the node argument.
        
        Recursive method which traverses the rest of the attribute hierarchy
        (represented as a tree) from the input node. This method considers every
        outlying datapoint in the given attribute space (determined by the input node
        object's attribute instance variable).
        
        Argument:
            node (Attribute_Node): node object of the attribute hierarchy
        """
        
        try:
            #create nontrivial column in outlier df for this node
            node.outliers.insert(1,'nontrivial',[np.NaN]*len(node.outliers.values))
        except:
            #print 'nontrivial column already created for node: {}'.format(list(node.attributes))
            pass
        
        #construct outlier set name and extract all outlier timestamps        
        outlier_set_name = '_'.join(node.attributes)+'_outlier'        
        node_outliers = node.outliers[ node.outliers[outlier_set_name] == 1 ].loc[:,outlier_set_name]
        
        #iterate over all outlying datapoints in this node
        for outlier in node_outliers.iteritems():
            
            #extract timestamp
            ts = outlier[0]
            
            #traverse the rest of the tree, to see if this data point is an outlier at any sub-attribute space
            #if it is, classify this datapoint at this node as NOT non-trivial (i.e. trivial)
            current_nontrivial_flag = 1
            i = 0
            num_children = len(node.children)       
            
            while current_nontrivial_flag == 1 and i < num_children:
                
                current_child = node.children[i]
                current_nontrivial_flag = self.inner_tree_traversal(current_child,current_nontrivial_flag,ts)                
                i += 1                
            
            #update nontrivial flag value
            node.outliers.loc[ts,'nontrivial'] = current_nontrivial_flag
        
        
        #Terminating condition/base case -- when the current node's children list
        #is empty then stop recursively calling the method (i.e. attempting to
        #deeper in the tree)
        if node.children == []:
            return
        
        #recursive call -- traverse over all the children of the current node        
        for child in node.children:
            self.outer_tree_traversal(child)
            
    def inner_tree_traversal(self, node, current_nontrivial_flag, ts):
        """Traverse sub-attribute spaces of the attribute hierarchy (i.e. tree) starting at the node argument.
        
        Recursive method which traverses all sub-attribute spaces of the tree starting
        at the node object argument passed in. The timestamp (i.e. datapoint) at each
        subspace is checked to see if it is also an outlier. If a timestamp is considered
        to be an outlier in a subspace then the original timestamp (of higher dimension attribute space)
        is flagged as trivial.
        Non-trivial outlier defn: Suppose that P is an outlier in the attribute space A_p. P is a
        non-trivial outlier in A_p if P is not an outlier in any proper subspace B of A_p.
        
        Arguments:
            node (Attribute_Node): object representing an attribute in the attribute hierarchy
            current_nontrivial_flag (int): an integer (0 or 1) which represents the current
                trivial status of the original datapoint passed in
            ts (datetime) - timestamp representing the original datapoint passed in
        
        Returns:
        inner_tree_traversal (method): recursive call to traverse the children node
            objects of the node obj passed in. Eventually either a 0 or 1 will be returned when
            maximum depth has been reached or when the timestamp has been classified as trivial.
        """
        
        #construct outlier column name
        outlier_set_name = '_'.join(node.attributes)+'_outlier'
        
        #extract outlier timestamps in the current node        
        outlier_timestamps = [i[0] for i in node.outliers[ node.outliers[outlier_set_name] == 1 ].loc[:,outlier_set_name].iteritems()]         
             
        #check if timestamp passed in is also an outlying data point in the current attribute space
        if ts in outlier_timestamps:
            current_nontrivial_flag = 0
        else:
            current_nontrivial_flag = 1
            
        # terminating conditions
        if node.children == []:
            
            return current_nontrivial_flag

        elif current_nontrivial_flag == 0:

            return current_nontrivial_flag

        else:
            #loop over all children objects and traverse the rest of the tree
            for child in node.children:
                return self.inner_tree_traversal(child,current_nontrivial_flag,ts)

    
    def classify_outliers_trivial_nontrivial(self):
        """Classify outliers as trivial or nontrivial.
        
        Driver method for trivial/nontrivial datapoint classification. Makes
        initial call to outer_tree_traversal method
        """

        self.outer_tree_traversal(self.root)
