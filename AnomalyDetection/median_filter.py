"""
Median Filter anomaly detection model.
"""

import pandas as pd

class Median_Filter:
    '''Object that applies the median filter model for outlier detection.
    
    Keyword Args:
        window_size (int): the number of consecutive values that comprise the rolling window used in the algorithm.
        threshold (int/float): the critical anonamly score value if exceeded causes a data point to be flagged as an outlier.
        output_csv (bool): flag denoting whether or not to output a CSV file with model results.
        plot (bool): flag denoting whether or not to plot the model results.
            
    Attributes:
        window_size (int): the number of consecutive values that comprise the rolling window used in the algorithm.
        threshold (int/float): the critical anonamly score value if exceeded causes a data point to be flagged as an outlier.
        output_csv (bool): flag denoting whether or not to output a CSV file with model results.
        plot (bool): flag denoting whether or not to plot the model results.
    '''
    
    def __init__(self, window_size=5, threshold=10, output_csv=False, plot=False,
                 output_type='scores', params=None):
        '''
        Initializes object and model parameters as instance variables.
        '''
        
        #I/O variables
        self.output_csv = output_csv        
        
        #Model parameters
        self.threshold = threshold
        self.window_size = window_size
        
        #plot flag
        self.plot = plot
        
        if params != None:
            self.threshold = params['threshold']
            self.window_size = params['window_size']
        
    def __str__(self):
        
        return 'Median_Filter'
        
    def __repr__(self):
        
        return 'Median_Filter(window_size={}, threshold={})'.format(self.window_size, self.threshold)
                
    def apply_(self, data, cols=None, output_type='scores'):
        '''
        Apply median filter model to all cols in data, return dataframe with all steps.
        
        Applies the median filter model to all columns specified in the self.columns instance variable.
        The model follows these steps:
        1. calculate the rolling median with rolling window size specified in the self.window_size variable
        2. calculate the difference between the rolling median calcualted in (1) and the actual value in question
        3. calculate the rolling median of the differences calculated in (2)
        4. calculate the score as the difference between the signal & the rolling median divided by the median difference in the window
        5. generate outlier flags based on the score and the threshold specified by the self.threshold variable        
        
        Arguments:
            data (DataFrame): the input dataset
        
        Keyword Arguments:
            cols (List[str]): the specific columns in data to run the median filter
                on.
            output_type (str): one of {'scores', 'full'} denoting the completeness of the
                dataframe returned
        
        Returns:
            df (DataFrame): model output
        '''
        
        #instantiate df variable
        df = data.copy()
            
        #instantiate columns variable
        columns = cols if cols != None else df.columns.values
        
        #loop over all columns, apply median filter model to each column
        for col in columns:
            
            #calculate rolling median
            df[ str(col)+'_rm' ] = pd.rolling_median(df[col], window=self.window_size, min_periods=self.window_size)
            
            #calculate the difference between the rolling median and the actual value            
            df[ str(col)+'_rm_dif' ] = abs(df[ str(col)+'_rm' ] - df[ col ])
            
            #calculate the rolling median of the above step
            df[ str(col)+'_rm_dif_rm' ] = pd.rolling_median(df[ str(col)+'_rm_dif' ], window=self.window_size, min_periods=1)
            
            #calculate the score: difference between signal & rolling median divided by the median difference in the window           
            df[ str(col)+'_mf_score' ] = df[ str(col)+'_rm_dif' ] / df[ str(col)+'_rm_dif_rm' ]
            
            # account for invalid values
            # if model results in 0/0 then set score = 1.0
            df.ix[ (df[str(col)+'_rm_dif_rm'] == 0) & (df[str(col)+'_rm_dif'] == 0),str(col)+'_mf_score' ] = 1.0

            # if model results in X/0 then set score = X/0.001
            df.ix[ (df[ str(col)+'_rm_dif_rm' ] == 0) & (df[str(col)+'_rm_dif'] != 0.0), str(col)+'_mf_score' ] = df.ix[ (df[ str(col)+'_rm_dif_rm' ]== 0) & (df[str(col)+'_rm_dif'] != 0), str(col)+'_rm_dif'] / 0.001            
            
            #generate flags
            df[ str(col)+'_mf_outlier_flag' ] = [1 if df.ix[i,str(col)+'_mf_score'] >= self.threshold else 0 for i in range(len(df[col].values))]
            
            #plot results
            if self.plot:
                df[ [col,str(col)+'_mf_score'] ].plot(subplots=True)
            
            #output csv
            if self.output_csv:
                df.to_csv(self.read_table_name+'_'+str(col)+'_mf_output.csv')
        
        # format output
        # extract all score columns
        if output_type == 'scores':
            output_cols = []
            for col in df.columns.values:
                if '_mf_score' in col:
                    output_cols.append(col)
                    
        df = df[ output_cols ].dropna()
        
        # scale results
        df = self.scale(df)
        
        #return dataframe
        return df
    
    def scale(self, df, cols=None, minimum=1, maximum=10):
        '''Convert values in df to a new range while maintaing original ratio.
        
        Arguments:
            df (DataFrame): input dataset
        
        Keyword Arguments:
            cols (List[str]): list of columns to scale. If None, all are used.
            minimum (int): minimum value in new range
            maximum (int): maximum value in new range
        
        Returns:
            scaled_df (DataFrame): dataframe with scaled values.
        '''

        #discern cols argument    
        if cols == None:
            cols = df.columns.values
        
        #initialize new dataframe to hold output
        scaled_df = pd.DataFrame(index=df.index)
        
        #loop over all columns        
        for col in cols:
            
            #replace NaN with min value
            df[col].fillna(value=1.0, inplace=True)
            
            #save old max and min values
            old_min_val = min(df[col])
            old_max_val = max(df[col])
        
            #calculate ranges
            old_range = old_max_val - old_min_val
            new_range = maximum - minimum    
            
            #check for no range
            if old_range == 0:
                df[col] = minimum
            else:
                #apply conversion
                cols = str(col)
                df[ col+'1'] = df[col] - old_min_val
                df[ col+'2'] = df[col+'1']*new_range
                df[ col+'3'] = df[col+'2']/old_range
                df[ col ] = df[col+'3']+minimum
                
            scaled_df = scaled_df.join(df[col])
        
        return scaled_df
        
        
    def test(self, test_df, columns=None):
        '''Test the median filter model.
        
        Method used for unit tests -- calculates the outlier score for each data point
        in each column of the test dataset (unless otherwise specified by the columns
        keyword argument). Note each column represents a series and that this model
        is one dimensional -- so each "cell" in the input dataframe will have a corresponding
        score associated with it in the output dataframe.
        
        Arguments:
            test_df (DataFrame): the input test dataset
        
        Keyword Arguments:
            columns (List[str]): list of the columns to run the median filter model on
        
        Returns:
            score_df (DataFrame): model scores output dataset
        '''
        
        #construct list of original column names
        test_df_cols = test_df.columns.values
        
        #apply median filter model
        output_df = self.apply_(data=test_df, cols=columns)
        
        #construct list of score column names
        if columns == None:
            score_cols = [col+'_mf_score' for col in test_df_cols]
        else:
            score_cols = [col+'_mf_score' for col in columns]
            
        #return scores
        score_df = output_df[score_cols]
        return score_df


        