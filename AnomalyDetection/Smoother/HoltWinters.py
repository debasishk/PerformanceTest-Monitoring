# Project: Predictive Analytics
# Author: Debasish Kanhar
# UserID: BC03421


from __future__ import division
from sys import exit
from math import sqrt
from numpy import array
from scipy.optimize import fmin_l_bfgs_b


class HoltWinters:
    """
    Holt-Winters method for decomposing time series. See

    http://robjhyndman.com/hyndsight/hw-initialization/

    for further information.
    """
    def __init__(self, full,  **kwargs):
        self.full = full
        kwargs = kwargs['kwargs']
        type = kwargs["type"]
        self.return_values = dict()
        if type == 'linear':
            data = kwargs['data']
            steps_future = kwargs['steps_forward']
            if len(kwargs) == 3:
                if self.full:
                    predictions, alpha, beta, rmse, u_int, l_int = self.linear(data, steps_future)
                else:
                    predictions, alpha, beta, rmse = self.linear(data, steps_future)
            elif len(kwargs) == 5:
                alpha = kwargs['alpha']
                beta = kwargs['beta']
                if self.full:
                    predictions, alpha, beta, rmse, u_int, l_int = self.linear(data,steps_future, alpha, beta)
                else:
                    predictions, alpha, beta, rmse = self.linear(data, steps_future, alpha, beta)
            else:
                raise TypeError("Exactly either 3 parameters or 5 parameters should be passed. Passed number {}".format(len(kwargs)))
            if self.full:
                self.return_values = {'predictions': predictions, 'alpha': alpha, 'beta': beta, 'rmse': rmse, 'upper': u_int, 'lower': l_int}
            else:
                self.return_values = {'predictions': predictions, 'alpha': alpha, 'beta': beta, 'rmse': rmse}
        elif type == 'additive':
            data = kwargs['data']
            steps_future = kwargs['steps_forward']
            periods = kwargs['periods']
            if len(kwargs) == 2:
                predictions, alpha, beta, gamma, rmse = self.additive(data, steps_future, periods)
            elif len(kwargs) == 4:
                alpha = kwargs['alpha']
                beta = kwargs['beta']
                gamma = kwargs['gamma']
                predictions, alpha, beta, gamma, rmse = self.additive(data,steps_future, periods, alpha, beta, gamma)
            else:
                raise TypeError("Exactly either 4 parameters or 7 parameters should be passed. Passed number {}".format(len(kwargs)))
            self.return_values = {'predictions': predictions, 'alpha': alpha, 'beta': beta, 'gamma':gamma, 'rmse': rmse}
        elif type == 'multiplicative':
            data = kwargs['data']
            steps_future = kwargs['steps_forward']
            periods = kwargs['periods']
            if len(kwargs) == 2:
                predictions, alpha, beta, gamma, rmse = self.multiplicative(data, steps_future, periods)
            elif len(kwargs) == 4:
                alpha = kwargs['alpha']
                beta = kwargs['beta']
                predictions, alpha, beta, gamma, rmse = self.multiplicative(data,steps_future, periods, alpha, beta)
            else:
                raise TypeError("Exactly either 4 parameters or 7 parameters should be passed. Passed number {}".format(len(kwargs)))
            self.return_values = {'predictions': predictions, 'alpha': alpha, 'beta': beta, 'gamma':gamma, 'rmse': rmse}
        else:
            raise TypeError("Type of smoothing passed should be either 'linear' or 'additive' or 'multiplicative'")
        pass

    def get_values(self):
        """
        Returns:
            self.return_values (Dictionary of information): Dictionary storing predicted values, alpha, beta & gamme params & root mean square error. Also returns bounds depending on self.full flag param passed to class.
        """
        return self.return_values

    def __MAPE__(self, params, *args):
        """
        Method to calculate error function for Holt Winters which needs to be minimized.

        Args:
            params (Tuple): The parameters used to calculate mean error which needs to be minimized.
            args: Argument list storing 'Y' values, and type of Holt Winters model to apply ('linear'/'additive'/'multiplicative')
        Returns:
             mape (Float): Root Mean Squared Error which is to be minimized
        """

        Y = args[0]
        type = args[1]
        rmse = 0
        # print(Y)
        if type == 'linear':

            alpha, beta = params
            a = [Y[0]]
            b = [Y[1] - Y[0]]
            y = [a[0] + b[0]]

            for i in range(len(Y)):
                a.append(alpha * Y[i] + (1 - alpha) * (a[i] + b[i]))
                b.append(beta * (a[i + 1] - a[i]) + (1 - beta) * b[i])
                y.append(a[i + 1] + b[i + 1])

        else:

            alpha, beta, gamma = params
            m = args[2]
            a = [sum(Y[0:m]) / float(m)]
            b = [(sum(Y[m:2 * m]) - sum(Y[0:m])) / float(m ** 2)]

            if type == 'additive':

                s = [Y[i] - a[0] for i in range(m)]
                y = [a[0] + b[0] + s[0]]

                for i in range(len(Y)):

                    a.append(alpha * (Y[i] - s[i]) + (1 - alpha) * (a[i] + b[i]))
                    b.append(beta * (a[i + 1] - a[i]) + (1 - beta) * b[i])
                    s.append(gamma * (Y[i] - a[i] - b[i]) + (1 - gamma) * s[i])
                    y.append(a[i + 1] + b[i + 1] + s[i + 1])

            elif type == 'multiplicative':

                s = [Y[i] / a[0] for i in range(m)]
                y = [(a[0] + b[0]) * s[0]]

                for i in range(len(Y)):

                    a.append(alpha * (Y[i] / s[i]) + (1 - alpha) * (a[i] + b[i]))
                    b.append(beta * (a[i + 1] - a[i]) + (1 - beta) * b[i])
                    s.append(gamma * (Y[i] / (a[i] + b[i])) + (1 - gamma) * s[i])
                    y.append((a[i + 1] + b[i + 1]) * s[i + 1])

            else:

                exit('Type must be either linear, additive or multiplicative')

        mape = sum([abs(n-m)/m for m, n in zip(Y, y[:-1])])/len(Y)

        # rmse = sqrt(sum([(m - n) ** 2 for m, n in zip(Y, y[:-1])]) / len(Y))

        return mape

    def linear(self, x, fc, alpha=None, beta=None):
        """
        :param self: Container variable
        :param x: Time series and data before time 't'. Of type :
        :param fc: Number of steps to forecast in future
        :param alpha: alpha parameter of HW Double ES
        :param beta: beta parameter of HW Double ES
        :return:
        """

        Y = x[:]

        if alpha is None or beta is None:

            initial_values = array([0.3, 0.1])
            boundaries = [(0, 1), (0, 1)]
            type = 'linear'

            parameters = fmin_l_bfgs_b(self.__MAPE__, x0 = initial_values, args = (Y, type), bounds = boundaries, approx_grad = True)
            alpha, beta = parameters[0]

        a = [Y[0]]
        b = [Y[1] - Y[0]]
        y = [a[0] + b[0]]
        rmse = 0
        MAD = 0
        for i in range(len(Y) + fc):

            if i == len(Y):
                Y.append(a[-1] + b[-1])

            a.append(alpha * Y[i] + (1 - alpha) * (a[i] + b[i]))
            b.append(beta * (a[i + 1] - a[i]) + (1 - beta) * b[i])
            y.append(a[i + 1] + b[i + 1])
            if self.full:
                for k in range(1, i+1):
                    MAD += abs(Y[i] - y[i-1])
                MAD /= (i+1)

                gamma = max(alpha, beta)
                delta = 1 - gamma
                numerator = 1 + ((gamma/pow(1+delta, 3))*(1 + 4*delta + 5*pow(delta,2) + 2*gamma*(1 + 3*delta)*fc + 2*pow(gamma*fc, 2)))
                denominator = 1 + ((gamma/pow(1+delta, 3))*(1 + 4*delta + 5*pow(delta,2) + 2*gamma*(1 + 3*delta) + 2*pow(gamma, 2)))
                d = 1.25*sqrt(numerator/denominator)

                upper_interval = Y[-fc:] + 1.96*d*MAD
                low_interval = Y[-fc:] - 1.96*d*MAD

                return y[-fc:], alpha, beta, rmse, upper_interval, low_interval

            else:
                rmse = sqrt(sum([(m - n) ** 2 for m, n in zip(Y[:-fc], y[:-fc - 1])]) / len(Y[:-fc]))

                return y[-fc:], alpha, beta, rmse

    def multiplicative(self, x, m, fc, alpha=None, beta=None, gamma=None):

        Y = x[:]

        if (alpha is None or beta is None or gamma is None):

            initial_values = array([0.0, 1.0, 0.0])
            boundaries = [(0, 1), (0, 1), (0, 1)]
            type = 'multiplicative'

            parameters = fmin_l_bfgs_b(self.__MAPE__, x0 = initial_values, args = (Y, type, m), bounds = boundaries, approx_grad = True)
            alpha, beta, gamma = parameters[0]

        a = [sum(Y[0:m]) / float(m)]
        b = [(sum(Y[m:2 * m]) - sum(Y[0:m])) / float(m ** 2)]
        s = [Y[i] / a[0] for i in range(m)]
        y = [(a[0] + b[0]) * s[0]]
        rmse = 0

        for i in range(len(Y) + fc):

            if i == len(Y):
                Y.append((a[-1] + b[-1]) * s[-m])

            a.append(alpha * (Y[i] / s[i]) + (1 - alpha) * (a[i] + b[i]))
            b.append(beta * (a[i + 1] - a[i]) + (1 - beta) * b[i])
            s.append(gamma * (Y[i] / (a[i] + b[i])) + (1 - gamma) * s[i])
            y.append((a[i + 1] + b[i + 1]) * s[i + 1])

        rmse = sqrt(sum([(m - n) ** 2 for m, n in zip(Y[:-fc], y[:-fc - 1])]) / len(Y[:-fc]))

        return Y[-fc:], alpha, beta, gamma, rmse

    def additive(self, x, m, fc, alpha=None, beta=None, gamma=None):

        Y = x[:]

        if alpha is None or beta is None or gamma is None:

            initial_values = array([0.3, 0.1, 0.1])
            boundaries = [(0, 1), (0, 1), (0, 1)]
            type = 'additive'

            parameters = fmin_l_bfgs_b(self.__MAPE__, x0 = initial_values, args = (Y, type, m), bounds = boundaries, approx_grad = True)
            alpha, beta, gamma = parameters[0]

        a = [sum(Y[0:m]) / float(m)]
        b = [(sum(Y[m:2 * m]) - sum(Y[0:m])) / float(m ** 2)]
        s = [Y[i] - a[0] for i in range(m)]
        y = [a[0] + b[0] + s[0]]
        rmse = 0

        for i in range(len(Y) + fc):

            if i == len(Y):
                Y.append(a[-1] + b[-1] + s[-m])

            a.append(alpha * (Y[i] - s[i]) + (1 - alpha) * (a[i] + b[i]))
            b.append(beta * (a[i + 1] - a[i]) + (1 - beta) * b[i])
            s.append(gamma * (Y[i] - a[i] - b[i]) + (1 - gamma) * s[i])
            y.append(a[i + 1] + b[i + 1] + s[i + 1])

        rmse = sqrt(sum([(m - n) ** 2 for m, n in zip(Y[:-fc], y[:-fc - 1])]) / len(Y[:-fc]))

        return Y[-fc:], alpha, beta, gamma, rmse

    # Test Function. Currently prediction intervals for Holt Winters double (Linear) forecasting.
    # To be extended for all types of smoothing in future, but not required now in scope of project
    # URL : http://it.minitab.com/support/documentation/Answers/doubleexponential.pdf
