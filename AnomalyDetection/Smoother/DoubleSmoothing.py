# Project: Predictive Analytics
# Author: Debasish Kanhar
# UserID: BC03421

from AnomalyDetection.Smoother.HoltWinters import HoltWinters
from datetime import datetime, timedelta
import pandas as pd
from math import ceil, exp
import numpy as np
import os, csv
from utils.mkdir import mkdir


class DoubleSmoothing(object):
    """
    Class to implement Double Smoothing using Holt Winters for predicting values.

    Methods:
        apply_ : Apply Double Smoothing to dataset. Inputs dataset where Double Smoothing to be applied, and returns smoothed values.
        __driver__ : Driver method for Double Smoothing.
        __get_data__ : Method to store passed data as instance attribute of class.
        __forecast__ : Method to initialize Holt Winters Double Smoothing to find out predicted/smoothed values.
        __rolling_forecast__ : The method to initialize forecasting based on DS on rolling basis. Creates a rolling window, fetches data for that window, and implements DS on that data.
        get_forecast : This method returns final smoothed out values which was saved as instance attribute.
        __filters__ : Apply filters to smoothed out values. If # of deviations exceed threshold, raise error.
    """
    def __init__(self, params, out_path=None, data=pd.DataFrame(), test_data=False, plot=False, full=False):

        # Passed params
        self.lookback_period = params['lookback']
        self.num_steps = params['num_steps']

        self.csv_in = False
        self.tmpData = data

        self.full = full

        # Inbuilt params
        self.resolution = 1
        self.min_data = 0
        self.fmt = "%m-%d-%y %H:%M"
        self.transformation = 'natural_log'
        self.application = 'msp_customer_transactions_a'

        # File names
        self.fname = '../../../Datasets/msp_order_customer_transactions_manipulated.csv'

        if hasattr(params, 'metric'):
            self.metric = params['metric']

            self.out_file = out_path + '{}\\'.format(self.metric)

        else:
            self.out_file = "Results\\HW\\"

        self.parent_dir = out_path


        # Functions
        if len(data.index):
            # When input data is passed, call driver() method.
            self.__driver__()
        pass

    def apply_(self, data=pd.DataFrame()):
        """
        Applies Double Smoothing on rolling basis to find out one step ahead's prediction

        Args:
            data (Pandas DataFrame): Input dataset which needs to be smoothed out, and predicted values to be found.

        Returns:
            outDF (Pandas DataFrame): The resultant dataframe, storing 'actual', 'predicted' and 'difference' columns indexed by timestamp
        """

        self.tmpData = data

        self.metric = data.columns.tolist()[0]

        self.__driver__()

        outDF = self.get_forecast()

        return outDF

    def __driver__(self):
        """
        Driver method for Double smoothing class.

        Returns:
            None
        """
        self.__get_data__()
        self.__rolling_forecast__()
        pass

    def __get_data__(self):
        """
        Method to fetch passed data and save it as Instance Attribute.

        Attributes:
            self.data (Pandas DataFrame): The input dataset passed to class either through initialization, or through apply_ method.
        Returns:
            None
        """
        self.data = self.tmpData
        pass

    def __forecast__(self, subdata, row):
        """
        Method to initialize Prediction based on Double Smoothing. Used the passed sub-data to predict one step in future.

        Args:
            subdata (Pandas Series): The sub-data on which Double Smoothing to be applied to predict one step in future.
            row (Pandas Series): The data corresponding to current timestamp. Need to calculate smoothed out values (Difference)

        Returns:
            prediction (Dictionary of Float) : The predicted value for one step ahead based on Double Smoothing
            future_data-prediction (Pandas Series): Pandas Series of single element storing difference b/w actual and predicted value for corresponding timestamp.
        """

        if type(subdata) is pd.core.frame.DataFrame:
            subdata = subdata.values

        if type(row) is pd.core.frame.DataFrame:
            row = row.values

        future_data = row

        # For rolling simulation:
        timeseries = subdata
        # timeseries.replace(to_replace=0, value=1, inplace=True)

        data = timeseries.tolist()

        for i in range(len(data)):
            if type(data[i]) is list:
                if len(data[i]) == 1:
                    data[i] = data[i][0]

        params = {"type": 'linear', "data": data, "steps_forward": self.num_steps}

        HW = HoltWinters(self.full, kwargs=params)
        return_values = HW.get_values()

        prediction = return_values['predictions'][self.num_steps-1]
        # print("prediction", prediction)

        if self.full:
            alpha = return_values['alpha']
            beta = return_values['beta']
            rmse = return_values['rmse']
            upper = return_values['upper']
            lower = return_values['lower']
            upper = exp(upper)
            lower = exp(lower)

            normal_predictions = [ceil(upper), ceil(exp(prediction)), ceil(future_data), ceil(lower)]
            normal_predictions = np.array(normal_predictions).reshape(1,4)
            predictions = pd.DataFrame(data=normal_predictions,
                                   columns=['upper_bound', 'prediction', 'actual', 'lower_bound'])
        else:
            alpha = return_values['alpha']
            beta = return_values['beta']
            rmse = return_values['rmse']

        return prediction, future_data - prediction

    def __rolling_forecast__(self):
        """
        This method is used to initialize forecast based on rolling window basis. Fetch data for last 'n' timestamp, and use it to predict one step in future.

        Attributes:
            self.outDF (Pandas DataFrame): The smoothed out values for passed dataset.

        Returns:
            None
        """

        distance = pd.DataFrame()
        mkdir(self.out_file)

        ctr = 0
        for index in self.data.index.tolist():
            row = self.data.ix[index]
        # for index, row in self.data.iteritems():

            if ctr > 2:
                # Selects the sublist to use for smoothing to predict forward value
                n = min(ctr, self.lookback_period)
                subdata = self.data[ctr-n:ctr]

                self.forecast_df = pd.DataFrame()

                forecast, difference = self.__forecast__(subdata, row)

                # print type(forecast['prediction'].values[0]), type(difference), type(row)
                # print (forecast['prediction'].values[0]), (difference), (row)

                tmpData = np.array((forecast, difference, row), dtype=float)
                tmpData = tmpData.reshape(1,-1)
                # print tmpData
                tmpDF = pd.DataFrame(data=tmpData, columns=['prediction', 'difference', 'actual'], index=[index])

                distance = distance.append(tmpDF)

                # if os.path.exists(self.out_file + 'output.csv'):
                #     with open(self.out_file + 'output.csv', 'a+') as f:
                #         distance.to_csv(f, header=False)
                # else:
                #     with open(self.out_file + 'output.csv', 'a+') as f:
                #         # wr = csv.writer(f, delimiter=',', lineterminator='\n')
                #         # wr.writerow(['timestamp', 'predictions', 'difference', 'actual'])
                #         distance.to_csv(f, header=True, index_label='timestamp', index=True)

            ctr += 1

        self.outDF = distance

        pass

    def get_forecast(self):
        """ Returns final output

        Returns:
            self.outDF (Pandas Dataframe): Returns the results from Kalman Filters stored in self.outDF
        """

        return self.outDF

    def __filters__(self, distance):

        if self.min_data <= distance.shape[0] < self.window_size:
            window = self.forecast_df.shape[0]
            mean = np.mean(distance.iloc[-1*window:])
            stdev = np.std(distance.iloc[-1*window:])
        else:
            window = self.window_size
            mean = np.mean(distance.iloc[-1*window:])
            stdev = np.std(distance.iloc[-1*window:])

        new_diff = distance[-1] - mean
        num_stdev = abs(new_diff/stdev)

        new_cols = [mean, stdev, new_diff, num_stdev]

        return new_cols


def main():
    lookback_days = 5
    num_steps = 1
    start_simulation = datetime(2015, 7, 1, 00, 00, 00)
    end_simulation = datetime(2015, 7, 5, 0, 00, 00)
    date_start = datetime(2015, 6, 15, 00, 00, 00)
    date_end = datetime(2015, 7, 6)
    threshold = 15
    full = False
    window = 20

    obj = DoubleSmoothing
    obj(lookback_days, num_steps, start_simulation, end_simulation, date_start, date_end, threshold, window, full)

if __name__ == '__main__':
    main()
