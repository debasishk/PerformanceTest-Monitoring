from dataprep import DataPreparation
from config import *

import numpy as np
import math
import sys
from operator import itemgetter
import csv
import pandas as pd


class AngleBasedOD(object):
    def __init__(self, N=200, kNN=100, topK=50, implementation='fast'):
        self.data_obj = DataPreparation()

        self.N = N
        self.kNN = kNN
        self.topK = topK
        self.implementation = implementation

        if __name__ == '__main__':
            self.__driver__()

        pass

    def apply_(self, data=pd.DataFrame()):
        if type(data) is dict:
            dict_data = True
        elif type(data) is pd.core.frame.DataFrame:
            dict_data = False
        else:
            raise TypeError("Passed data type can only be dictionary type with key as server names, or pandas dataframe with dataframe storing data for single server")

        if dict_data:
            results = self.prepare_data_(data)
        else:
            if self.implementation == 'normal':
                results = self.prepare_model_(data)
            elif self.implementation == 'fast':
                results = self.FastABOD(data)
            else:
                raise AssertionError(
                    'Implementation parameter for class can only be either in [normal, fast]. Passed value is {}'.format(
                        self.implementation))

            results.to_csv('{}_results_abod.csv'.format(self.implementation))

        return results

    def __driver__(self):
        data = self.fetch_data_()
        self.prepare_data_(data)
        pass

    def fetch_data_(self):
        data = self.data_obj.fetch_()
        return data

    def prepare_data_(self, data):
        resultSet = dict()

        for server in data.keys():
            data_for_server = data[server]

            if self.implementation == 'normal':
                results = self.prepare_model_(data_for_server)
            elif self.implementation == 'fast':
                results = self.FastABOD(data_for_server)
            else:
                raise AssertionError(
                    'Implementation parameter for class can only be either in [normal, fast]. Passed value is {}'.format(
                        self.implementation))

            results.to_csv("{}_results_abod_{}.csv".format(self.implementation, server))
            resultSet[server] = results

        return resultSet

    def ApproxABOF(self, pointsList, A, indexA, distTable, kNN):
        """
        calculating the approximate ABOF of point A
        """
        pointsList = np.array(pointsList)
        distTable = np.array(distTable)
        # get the index of k nearest neighbor points of point A
        kNNIndex = self.getkNNIndex(pointsList, A, indexA, kNN, distTable)
        # compute Approximate ABOF
        varList = []
        i = 0
        for i in range(len(kNNIndex)):
            indexB = kNNIndex[i]
            B = pointsList[indexB]
            if indexA > indexB:
                AB = distTable[indexA][indexB]
            elif indexA < indexB:
                AB = distTable[indexB][indexA]

            j = 0
            for j in range(i + 1):
                if i == j:
                    continue
                indexC = kNNIndex[j]
                C = pointsList[indexC]
                if indexA > indexC:
                    AC = distTable[indexA][indexC]
                elif indexA < indexC:
                    AC = distTable[indexC][indexA]

                angle_BAC = self.angleBAC(A, B, C, AB, AC)
                try:
                    tmp = angle_BAC / float(math.pow(AB * AC, 2))
                except ZeroDivisionError:
                    sys.exit('ERROR\tApproxABOF\tfloat division by zero!')
                varList.append(tmp)
        variance = np.var(varList)
        return variance

    def LB_ABOF(self, pointsList, A, index, kNN, distTable):
        """
        calculate the LB-ABOF of point A
        """
        kNNIndex = self.getkNNIndex(pointsList, A, index, kNN, distTable)
        Nk = []
        minuend_molecule = 0.0
        subtrahend_molecule = 0.0
        i = 0
        for i in range(len(kNNIndex)):
            indexB = kNNIndex[i]
            if index == indexB:
                continue
            B = pointsList[indexB]
            Nk.append(B)
            if index > indexB:
                AB = distTable[index][indexB]
            else:  # index < indexB
                AB = distTable[indexB][index]
            j = 0
            for j in range(i + 1):
                if i == j:
                    continue
                indexC = kNNIndex[j]
                if index == indexC:
                    continue
                C = pointsList[indexC]
                if index > indexC:
                    AC = distTable[index][indexC]
                else:
                    AC = distTable[indexC][index]

                angle_BAC = self.angleBAC(A, B, C, AB, AC)
                try:
                    tmp = angle_BAC / (math.pow(AB * AC, 3))  # <AB, AC> / (|AB|^3 * |AC|^3)
                except ZeroDivisionError:
                    sys.exit('ERROR\tLB_ABOF\tdistance can not be zero!')
                minuend_molecule += math.pow(tmp, 2)
                subtrahend_molecule += tmp
        r2 = self.R2(pointsList, Nk, A, index, distTable)
        deno = self.Denominator(pointsList, A, index, distTable, 1)
        try:
            minuend = minuend_molecule / deno
            subtrahend = math.pow(((subtrahend_molecule + r2) / deno), 2)
        except ZeroDivisionError:
            sys.exit('ERROR\tLB_ABOF\tdeominator can not be zero!')
        LBABOF_A = minuend - subtrahend
        return LBABOF_A

    ####################ABOD algorithm implement####################
    def ABOF(self, pointsList, A, index, distTable):
        """
        calculate the ABOF of A = (x1, x2, ..., xn)
        """
        pointsList = np.array(pointsList)
        i = 0
        varList = []
        for i in range(len(pointsList)):
            if i == index:  # ensure A != B
                continue
            B = pointsList[i]
            if index < i:
                AB = distTable[i][index]
            else:  # index > i
                AB = distTable[index][i]

            j = 0
            for j in range(i + 1):
                if j == index or j == i:  # ensure C != A && B != C
                    continue
                C = pointsList[j]
                if index < j:
                    AC = distTable[j][index]
                else:  # index > j
                    AC = distTable[index][j]

                angle_BAC = self.angleBAC(A, B, C, AB, AC)

                # compute each element of variance list
                try:
                    tmp = angle_BAC / float(math.pow(AB * AC, 2))
                except ZeroDivisionError:
                    sys.exit('ERROR\tABOF\tfloat division by zero!')
                varList.append(tmp)
        variance = np.var(varList)
        return variance

    def angleBAC(self, A, B, C, AB, AC):  # AB AC mold
        """
        calculate <AB, AC>
        """
        vector_AB = B - A  # vector_AB = (x1, x2, ..., xn)
        vector_AC = C - A
        mul = vector_AB * vector_AC  # mul = (x1y1, x2y2, ..., xnyn)
        dotProduct = mul.sum()  # dotProduct = x1y1 + x2y2 + ... + xnyn
        try:
            cos_AB_AC_ = dotProduct / (AB * AC)  # cos<AB, AC>
        except ZeroDivisionError:
            sys.exit('ERROR\tangleBAC\tdistance can not be zero!')
        if math.fabs(cos_AB_AC_) > 1:
            print('A\n', A)
            print('B\n', B)
            print('C\n', C)
            print('AB = %f, AC = %f' % (AB, AC))
            print('AB * AC = ', dotProduct)
            print('|AB| * |AC| = ', AB * AC)
            sys.exit('ERROR\tangleBAC\tmath domain ERROR, |cos<AB, AC>| <= 1')
        angle = float(math.acos(cos_AB_AC_))  # <AB, AC> = arccos(cos<AB, AC>)
        return angle

    def prepare_model_(self, data):
        print("Some information for comparing.. Number of elements/rows = ", len(data.index))
        print("I'm preparing model now!")
        pointsList = self.attributeSelection(data)
        print("I just selected attributes")
        distTable = self.computeDistMatrix(pointsList)
        print("I just computed distance matrix")
        # (step 2) 	Compute LB-ABOF for each point A ∈ D.
        # (step 3) 	Organize the database objects in a candidate list ordered ascendingly
        #			w.r.t. their assigned LB-ABOF.
        DictLBABOF = {}
        i = 0
        for A in pointsList:
            LBABOF_A = self.LB_ABOF(pointsList, A, i, self.kNN, distTable)
            DictLBABOF.setdefault(i, LBABOF_A)
            i += 1
        candidateList = self.dictSortByValue(DictLBABOF)  # sorted LB-ABOF list ascendingly

        print("We have results. Number of rows in results = ", len(candidateList))
        # print('candidateList:\ni\tLB-ABOF\n')
        # for (k, v) in candidateList:
        #     print('%d\t%f' % (k, v))

            # (step 4) 	Determine the exact ABOF for the first @topK objects in the candidate
            #			list, Remove them from the candidate list and insert into the current
            #			result list.
        resultList = {}  # result list: outlier
        j = 0
        for j in range(len(candidateList)):
            (k, v) = candidateList.pop(0)
            A = pointsList[k]
            ABOFValueOfA = self.ABOF(pointsList, A, k, distTable)
            resultList[k] = ABOFValueOfA
            # (step 6)	if the largest ABOF in the result list < the smallest approximated
            #			ABOF in the candidate list, terminate; else, proceed with (step 5).
            # (step 5)	Remove and examine the next best candidate C from the candidate list
            #			and determine the exact ABOF, if the ABOF of C < the largest ABOF
            #			of anobject A in the result list, remove A from the result list and
            #			insert C into the result list.
        resultList = self.dictSortByValue(resultList)
        # result = pd.DataFrame.from_dict(resultList)
        # print(result)
        # print(len(resultList))
        # print(candidateList)
        # MIN_LBABOF = candidateList[0][1]
        # MAX_ABOF = resultList[self.topK - 1][1]
        # while MAX_ABOF > MIN_LBABOF:
        #     (Ck, Cv) = candidateList.pop(0)
        #     C = pointsList[Ck]
        #     ABOF_C = self.ABOF(pointsList, C, Ck, distTable)
        #     if ABOF_C < MAX_ABOF:
        #         del resultList[len(resultList) - 1]
        #         resultList.append((Ck, ABOF_C))
        #         resultList = sorted(resultList, key=lambda t: t[1], reverse=False)
        #
        #         MIN_LBABOF = candidateList[0][1]
        #         MAX_ABOF = resultList[self.topK - 1][1]
        # print(len(resultList))
        # print('resultList:\ni\tABOF\n')
        # for (k, v) in resultList:
        #     print('%d\t%f' % (k, v))

        # saving result...
        outlier = []
        for k, v in resultList:
            outlier.append(k + 1)  # In reality, outlier ID start with 1; in pointsList, it start with 0
        # self.writeFile(outlier, "output_abod.csv")

        results = pd.DataFrame.from_dict(resultList)
        results.columns = ['i', 'ABOF']

        return results

    def FastABOD(self, data):
        """
        Fast ABOD algorithm implementation
        """
        print("Length od data", len(data))
        pointsList = self.attributeSelection(data)
        distTable = self.computeDistMatrix(pointsList)
        DictApprABOF = {}
        i = 0
        for A in pointsList:
            apprABOF = self.ApproxABOF(pointsList, A, i, distTable,self.kNN)
            DictApprABOF.setdefault(i + 1, apprABOF)
            i += 1

        outlierList = self.dictSortByValue(DictApprABOF)
        print("Fast implementation")

        result = pd.DataFrame(data=outlierList, columns=['id', 'ABOD'])

        return result

    def writeFile(self, data, fSrc):
        """
        save @data into @fSrc
        """
        with open(fSrc, 'w') as f:
            outcsv = csv.writer(f, delimiter=',')
            outcsv.writerow(data)

    ####################Fast ABOD algorithm implemement####################
    def getkNNIndex(self, pointsList, A, index, kNN, distTable):
        """
        get k nearest neighbor of point A
        """

        distDictOfA = {}
        i = 0
        for i in range(len(pointsList)):
            if i == index:
                continue
            elif i > index:
                AB = distTable[i][index]
            else:  # i < index
                AB = distTable[index][i]
            distDictOfA[i] = AB

        sortedDistList = self.dictSortByValue(distDictOfA)
        kNNIndex = []  # k nearest neighbor point list
        j = 0

        for k, v in sortedDistList:
            if j < kNN:
                kNNIndex.append(k)
                j += 1

        return kNNIndex

    def R2(self, D, Nk, A, index, distTable):
        """
        calculate R2 in LB-ABOF
        """
        minuend = self.Denominator(D, A, index, distTable, 3)
        subtranend = self.Denominator(Nk, A, index, distTable, 3)
        return (minuend-subtranend)

    def Denominator(self, D, A, index, distTable, power):
        """
        calculate the denominator of ABOF, ApproxABOF and LB_ABOF
        """
        i = 0
        deno = 0
        for i in range(len(D)):
            if i == index:
                continue
            B = D[i]
            if index > i:
                AB = distTable[index][i]
            else:
                AB = distTable[i][index]
            j = 0
            for j in range(i + 1):
                if j == i or j == index:
                    continue
                C = D[j]
                if index > j:
                    AC = distTable[index][j]
                else:
                    AC = distTable[j][index]
                try:
                    tmp = 1 / (AB * AC)
                except ZeroDivisionError:
                    sys.exit('ERROR\tDenominator\tdistance can not be zero!')
                deno += math.pow(tmp, power)
        return deno

    def dictSortByValue(self, d, reverse=False):
        """
        sort the given dictionary by value
        """
        return sorted(d.items(), key=itemgetter(1), reverse=False)

    def attributeSelection(self, data):
        """
        Select the required attributes/columns/features for outlier detection

        Args:
            data (Pandas DataFrame): The data whose feature selection to be done

        Returns:
            result (Pandas DataFrame): The cleaned dataset
        """

        (row, column) = data.shape
        cols = data.columns.tolist()

        coord = []

        for i in range(column):
            if cols[i] in cols_to_del:
                # Remove columns 'id' & 'server'
                continue
            tmp = [data.ix[row][i] for row in range(row)]
            tmp = self.normalization(tmp)
            coord.append(tmp)

        result = np.transpose(coord)

        return result

    def normalization(self, rowDataList):
        npDataList = [float(s) for s in rowDataList]
        npDataList = np.array(npDataList)
        means = npDataList.mean()
        stdDev = npDataList.std()  # standard deviation
        normalizedList = []
        for x in npDataList:
            y = (x - means) / stdDev
            normalizedList.append(y)
        return normalizedList

    def computeDistMatrix(self, pointsList):
        """
        Compute distance of each pair of points and stored in distTable
        Args:
            pointsList:

        Returns:

        """

        pointsList = np.array(pointsList)
        (nrow, ncolumn) = pointsList.shape
        distTable = [[] for i in range(nrow)]  # define a row * row array
        i = 0
        for i in range(nrow):
            j = 0
            for j in range(i + 1):
                if i == j:
                    distTable[i].append(0.0)
                    continue
                mold_ij = self.euclDistance(pointsList[i], pointsList[j])
                distTable[i].append(mold_ij)

        return distTable

    def euclDistance(self, A, B):
        """
        calculate the euclidean metric of @A and @B
        A = (x1, x2, ..., xn)  are initialized
        """
        if len(A) != len(B):
            sys.exit('ERROR\teuclDistance\tpoint A and B not in the same space')
        vector_AB = A - B
        tmp = vector_AB ** 2
        tmp = tmp.sum()
        distance = tmp ** 0.5
        return distance


obj = AngleBasedOD()