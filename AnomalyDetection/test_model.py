# To test newly developed models developed

from AnomalyDetection.HCiS import HCiS
from dataprep import DataPreparation
import numpy as np

data_obj = DataPreparation()
data = data_obj.fetch_()
HCiS_obj = HCiS()

for k in data.keys():
    da = data[k]
    # da = da[['cpu', 'memory']]
    cols_to_include = [col for col in da.columns.tolist() if col != 'server']
    da = da[cols_to_include]

    scores = HCiS_obj.apply_(da)
    print(k)
    s = list(np.array(scores).flat)
    print(s)
