"""
Convex Hull model for outlier detection.
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull

class Convex_Hull:
    '''Object that applies the convex hull model for outlier detection.
        
    Keyword Arguments:
        depth (int): the maximum number of successive convex hulls to calculate
        output_csv (bool): flag denoting whether or not to output a CSV file with model results.
        plot (bool): flag denoting whether or not to plot the model results.
    
    Attributes:
        depth (int): the maximum number of successive convex hulls to calculate
        output_csv (bool): flag denoting whether or not to output a CSV file with model results.
        plot (bool): flag denoting whether or not to plot the model results.
    '''
    
    def __init__(self, depth=5, output_csv=False, plot=False, params=None):
        '''Initialize object and model parameters as instance variables.'''
        
        #I/O variables
        self.output_csv = output_csv
        
        #Model parameters
        self.depth = depth
        
        #plot flag
        self.plot = plot
        
        if params != None:
            self.depth = params['depth']
    
    def __str__(self):
        
        return 'Convex Hull'
    
    def __repr__(self):
        
        return 'Convex Hull(depth={})'.format(self.depth)

    
    def apply_(self, data, relationships=None):
        '''Apply convex hull model to all attribute sets in relationships arg.
        
        Driver method for convex hull outlier detection model. This method makes calls
        to all relevant methods needed to find the depth of each data point in
        the provided set of relationships.
        
        Arguments:
            data (DataFrame): the input dataset
        
        Keyword Arguments:
            relationships (List[List[str]]): 2D list of attribute sets to run the
                model on. For example, [ ['CPU', 'Memory'], ['CPU', 'Transactions'] ].
                If None then all columns in data are used.
        
        Returns:
            output_df (DataFrame): dataframe of convex hull depth levels for each
                attribute-set as separate columns.
        '''
        
        # initialize the output dataframe
        output_df = pd.DataFrame(index=data.index)
        
        #set relationships if not provided
        relationships = [data.columns.values] if relationships == None else relationships
        
        for col_set in relationships:

            # extract all metrics from input dataset to run through model            
            convex_hull_input = data[ col_set ]
            
            # apply the model
            temp_df = self.find_convex_hull(convex_hull_input, self.depth, col_set,
                                            self.plot, self.output_csv)
            
            #merge original dataframe with the output of current model
            output_df = output_df.join(temp_df)
            
        return output_df
    
    
    def find_convex_hull(self, data, depth, col_pair, plot=False, output_csv=False):
        '''Calculate successive convex hulls.        
        
        Finds the convex hull of the input dataset at successive depth levels.
        These levels are determined by finding the convex hull, removing these points that
        constitute the convex hull and then finding the convex hull of the remaining dataset.
        
        Arguments: 
            data (DataFrame): the input dataset
            depth (int): the number of successive convex hulls to calculate
            col_pair (List[str]): the attributes/metrics contained in data
        
        Keyword arguments:
            plot (bool): flag used to visualize results
            output_csv (bool): flag used to create output CSV file or not
        
        Returns:
            df_depth (DataFrame): Model output -- depth levels for the given
                attribute set
        '''
    
        #initialize df
        df = data    
    
        #get name of variables formatted
        col_header = '_'.join(col_pair)+'_depth'
        
        #create a copy of the initial dataset
        df_depth = df.copy()
        
        #check the dimensionality of the input data
        sufficient_dimensionality = self.check_dimensionality(df)
        
        #if the dimensionality is insufficient then no convex hull exists so set all
        #depth levels to maximum depth
        if not sufficient_dimensionality:
            df_depth[col_header] = depth
            print ('\tinsufficient dimensionality')
            return df_depth[col_header]            
        
        #initialize the depth column
        df_depth[col_header] = np.NaN
        
        #loop *depth* times
        for i in range(1,depth+1):
            
            #check for sufficient dimensionality of df (after removing NaNs from previous CH)
            sufficient_dimensionality = self.check_dimensionality(df)
            if not sufficient_dimensionality:
                print ('\tinsufficient dimensionality at depth:',i)
 
                #loop over all remaining NaNs in depth df
                null_df = df[ df_depth.isnull().any(axis=1) ]
                for index in null_df.index.values:
                    
                    #set all remaining NaNs to maximum depth (to produce the lowest score)
                    df_depth.set_value(index, col_header, depth)
                      
                #return resulting depth dataframe
                return df_depth[col_header]
            
            #check if all points have been included in a convex hull thus far
            #if so, break out of the loop
            if df.empty:
                break
            
            #check if there are enough data points to construct a convex hull
            #if not, break out of the loop
            if len(df) < 3:
                break
            
            hull = ConvexHull(df)
            
            #loop over vertices in convex hull
            for vertex in hull.vertices:
                
                #find datetime index
                index = df.iloc[vertex].name
                
                #set depth value
                df_depth.set_value(index,col_header,i)         
                
                #set row = None             
                df.iloc[vertex] = None
            
            #remove Null rows (in effect removes pts from convex hull)
            df = df.dropna()
        
        #Update all vertices not in any convex hull up to max depth to >=depth
        df_depth[col_header] = df_depth[col_header].replace(np.NaN,int(i))

        
        #write out csv file
        if output_csv:
            
            filename = 'ch_depth.csv'
            df_depth.to_csv(filename)
            
        if plot: #only supports 2D plotting rn
            
            x_col = col_pair[0]
            y_col = col_pair[1]
            
            title = 'Convex Hull: ' + ', '.join(col_pair)            
            
            attr1_vals = df[ x_col ]
            attr2_vals = df[ y_col ]
            
            color = ['r' if point == 1 else 'b' for point in list(df_depth[ col_header ].values)]
            
            fig = plt.figure()
            
            plt.scatter(attr1_vals,attr2_vals, c=color, s=40)
            plt.xlabel(x_col)
            plt.ylabel(y_col)
            plt.title(title)
            
        
        return df_depth[col_header]
    
    def check_dimensionality(self, df):
        '''
        Check the dataframe for implied reduced dimensionality.
        
        If the dataframe has 2 columns and 1 has all the same values then it is
        essentially a 1 dimensional dataset -- in this case we can't run the model.
        So this method returns False.
        
        Arguments:
            df (DataFrame): the input dataset
        
        Returns:
            sufficient_dimensionality (bool): flag denoting whether or not the 
                dimensionality of the dataframe is sufficent
        '''
        
        #instantiate a dict to hold number of unique vals in the df
        num_unique = {}        
        
        #loop over all columns
        for column in df.columns.values:
            
            #check the number of unique vals in this column
            #return False if the dataframe is essentially 1-dimensional
            unique_vals = df[ column ].unique()
            num_unique[column] = len(unique_vals)
            if len(unique_vals) == 1 and len(df.columns.values) <= 2:
                sufficient_dimensionality = False
                return sufficient_dimensionality
        
        # if loop finishes execution then the dataframe is good, return True
        sufficient_dimensionality = True
        return sufficient_dimensionality
    
    def scale(self, df, cols=None, minimum=1, maximum=10):
        '''Convert values in df to a new range while maintaing original ratio.
        
        Arguments:
            df (DataFrame): input dataset
        
        Keyword Arguments:
            cols (List[str]): list of columns to scale. If None, all are used.
            minimum (int): minimum value in new range
            maximum (int): maximum value in new range
        
        Returns:
            scaled_df (DataFrame): dataframe with scaled values.
        '''

        #discern cols argument    
        if cols == None:
            cols = df.columns.values
        
        #initialize new dataframe to hold output
        scaled_df = pd.DataFrame(index=df.index)
        
        #loop over all columns        
        for col in cols:
            
            #save old max and min values
            old_min_val = min(df[col])
            old_max_val = max(df[col])
        
            #calculate ranges
            old_range = old_max_val - old_min_val
            new_range = maximum - minimum        
            
            #apply conversion
            cols = str(col)
            df[ col+'1'] = df[col] - old_min_val
            df[ col+'2'] = df[col+'1']*new_range
            df[ col+'3'] = df[col+'2']/old_range
            df[ col ] = df[col+'3']+minimum
            scaled_df = scaled_df.join(df[col])
        
        return scaled_df


    def test(self, test_df, columns=None):
        '''
        Test the convex hull model.
        
        Method used for unit tests -- calculates the convex hull for the input
        dataframe and the specific columns of that dataframe
        
        Arguments:
            test_df (DataFrame): the input dataset
        
        Keyword arguments:
            columns (List[List[str]]): 2D list of attribute sets to run the model
                on. For example: [ ['CPU', 'Memory'], ['CPU', 'Transactions'] ].
                If None then all columns in test_df are used.
        
        Returns:
            test_output (DataFrame): dataframe of convex hull depth levels for each
                attribute-set as separate columns.
        '''
        
        test_output = self.apply_(test_df, columns)
        
        return test_output
        