from AnomalyDetection import MedianAbsoluteDeviation as MAD
from AnomalyDetection import LOF
from AnomalyDetection import FastFourierTransform as fft
from AnomalyDetection import ModelParams
from AnomalyDetection import UnivariateKalmanFilter
from AnomalyDetection import DoubleSmoothing

import pandas as pd
import datetime as dt


class ModelAggregation(object):
    def __init__(self):
        pass

    def apply_(self, data=None, smoother=None, models_to_use=dict(), return_dict=dict(), server=None):

        self.data = data
        self.smoother = smoother
        self.models_to_use = models_to_use

        self.new_col = data.columns.tolist()[0] + '_difference'

        scores, df = self.__driver__()

        if server is not None:
            # Server argument is passed only when parallel processing is implemented. Then use shared variable return_dict to store return values.
            if type(server) is str:
                return_dict[server] = scores
            elif type(server) is list:
                server_name = server[0]
                metric_name = server[1]
                return_dict[server_name] = dict()
                return_dict[server_name][metric_name] = scores
                print(return_dict)

        else:
            return scores

    def initialize_classes(self):
        self.ModelParams = ModelParams()

        if self.smoother == 'KF':
            self.smoother_obj = UnivariateKalmanFilter
        elif self.smoother == 'HW':
            self.smoother_obj = DoubleSmoothing
        elif self.smoother is None:
            self.smoother_obj = None

    def __driver__(self):
        self.initialize_classes()

        if self.smoother is not None:
            smoother_obj = self.initialize_smoother(self.smoother, self.smoother_obj)
            data = self.apply_smoother(smoother_obj)
            data = self.extract_value_from_smoother_predictions(data)
            self.data = data

        model_dict = self.prepare_model_dict()
        scores, df = self.prepare_models(model_dict)
        # print(scores)
        return scores, df

    def extract_value_from_smoother_predictions(self, data, cols_to_fetch='difference'):
        tmpData = data[[cols_to_fetch]]
        tmpData.rename(columns={'difference': self.new_col}, inplace=True)
        return tmpData

    def initialize_smoother(self, smoother_, smoother_obj):
        model_params = self.ModelParams.model_params

        params = model_params[smoother_]

        obj = smoother_obj(params=params)

        return obj

    def apply_smoother(self, smoother_obj):
        data = smoother_obj.apply_(self.data)
        return data

    def apply_model(self, model_obj):
        # print(type(self.data))
        ret = model_obj.apply_(self.data)

        # tmpDF = pd.DataFrame(data=ret, index=self.data.index.tolist(), columns=['score'])
        return ret

    def prepare_models(self, model_dict):
        score_dict_per_model = dict()

        df = pd.DataFrame()

        for k in model_dict.keys():
            v = model_dict[k]
            print("Applying {} model.".format(k))

            stn = dt.datetime.now()
            ret = self.apply_model(v)
            etn = dt.datetime.now()
            print("Time taken to apply {} model is {}".format(k, etn-stn))
            print(50*"-")
            ret.rename(columns={'score': '{}_score'.format(k)}, inplace=True)

            score_dict_per_model[k] = ret

            if df.shape[0] == 0:
                df = self.data.join(ret, how='right')
            else:
                df = df.join(ret, how='right')

        return score_dict_per_model, df

    def initialize_models(self, model_name, model_obj):

        model_params = self.ModelParams.model_params

        params = model_params[model_name]

        if type(model_obj) is type:
            # Check if the model object passed is initialized or not, if not, initialize it
            obj = model_obj(**params)
        else:
            # Else, return the initialized object to be used further
            obj = model_obj

        return obj

    def prepare_model_dict(self):

        if len(self.models_to_use) == 0:
            model_dict = dict()

            model_dict['FFT'] = fft
            model_dict['LOF'] = LOF
            model_dict['MAD'] = MAD

        else:
            model_dict = self.models_to_use

        for k, v in model_dict.items():
            model_dict[k] = self.initialize_models(k, v)

        return model_dict

if __name__ == '__main__':
    obj = ModelAggregation()