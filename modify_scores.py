from AnomalyDetection import ConvertScores
from AnomalyDetection import ScaleScores


class ModifyScores(object):
    def __init__(self):
        pass

    def apply_(self, data=None):
        z_score = self.convert_scores(data)
        scaled_score = self.scale_scores(z_score)


        k = list(scaled_score.keys())

        if len(k) == 1:
            key = k[0]
            ret = scaled_score[key]

        col = ret.columns.tolist()[0]

        ret.rename(columns={col: key}, inplace=True)

        return ret

    def convert_scores(self, data):
        convert_obj = ConvertScores()
        ret = convert_obj.apply_(data)

        return ret

    def scale_scores(self, data):
        scale_obj = ScaleScores(data)
        ret = scale_obj._scaled_scores_()

        return ret